<?php 
/*
	Template Name: Staff Calvary Church
*/
?>
<?php get_header(); ?>
	<main role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
		<section class="hero">
			<?php 
				if ( has_post_thumbnail() ) {
					the_post_thumbnail('full');
				} 
			?>
			<div class="hero__wrapper">
				<div class="hero__cont">
					<h2><?php the_field('hero_title'); ?></h2>
					<p><em><?php the_field('hero_description'); ?></em></p>
				</div>
			</div>
		</section>
		<section class="kidzone">
			
			<div class="blade blade__small">
				<div class="blade__cont">
					<h2><?php the_field('banner_title'); ?></h2>					
				</div>
			</div>
			<div class="blade">
				<ul class="team-grid">
					<?php
						$args = array(
							'post_type'			=> 'staff',
							'orderby'	 		=> 'menu_order',
							'posts_per_page' 	=> -1,
						);
						$products = new WP_Query( $args );
						if( $products->have_posts() ) {
							while( $products->have_posts() ) {
								$products->the_post();
								
								if(get_field('type_of_staff') == 'Pastors Director') {
					?>
									<li>
							<figure>
								<?php 
									if ( has_post_thumbnail() ) {
										the_post_thumbnail('full');
									} 
								?>
								<figcaption>
									<h3><?php the_title(); ?></h3>
									<h2><?php the_field('job_title'); ?></h2>
									<p><?php the_field('phone_number'); ?></p>
									<a href="mailto:<?php the_field('email'); ?>"><?php the_field('email'); ?></a>
								</figcaption>		
							</figure>
						</li>
					<?php
								}
					  		}
						}
						else { echo "Oh they aren't member of staff at this moment"; }
						wp_reset_query();
					?>

				</ul>
			</div>
			<div class="blade blade__small">
				<div class="blade__cont">
					<h2><?php the_field('secondary_staff_title'); ?></h2>
				</div>
			</div>
			<div class="blade">
				<ul class="team-grid">
					<?php
						$args = array(
							'post_type'			=> 'staff',
							'orderby'	 		=> 'menu_order',
							'posts_per_page' 	=> -1,
							
						);
						$products = new WP_Query( $args );
						if( $products->have_posts() ) {
							while( $products->have_posts() ) {
								$products->the_post();
								if(get_field('type_of_staff') == 'Support Staff') {
					?>
						<li>
							<figure>
								<?php 
									if ( has_post_thumbnail() ) :
										the_post_thumbnail('full');
									else: 
								?>
									 <img class="full-width-img" src="<?php echo get_template_directory_uri(); ?>/library/images/staff/placeholder-team.jpg" alt="Calvary Church | Connecting People with God" />
									<?php endif;?>
								<figcaption>
									<h3><?php the_title(); ?></h3>
									<h2><?php the_field('job_title'); ?></h2>
									<p><?php the_field('phone_number'); ?></p>
									<a href="mailto:<?php the_field('email'); ?>"><?php the_field('email'); ?></a>
								</figcaption>		
							</figure>
						</li>
						<?php
								}
					  		}
						}
					else { echo "Oh they aren't member of staff at this moment"; }
					wp_reset_query();
				?>
				</ul>
			</div>
			<div class="blade blade__small">
				<h2><?php the_field('board_title'); ?></h2>
				<p><?php the_field('board_description');?></p>
				<div class="board-members">
					<?php the_field('board_members'); ?>
				</div>
			</div>
		</section>
	</main>
<?php get_footer(); ?>
