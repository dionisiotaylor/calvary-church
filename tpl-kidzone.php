<?php 
/*
	Template Name: Kid Zone Calvary Church
*/

?>
<?php get_header(); ?>
	<main role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
		<section class="hero">
			<?php 
				if ( has_post_thumbnail() ) {
					the_post_thumbnail('full');
				} 
			?>
			<div class="hero__wrapper">
				<div class="hero__cont">
					<h2><?php the_field('hero_title'); ?></h2>
					<p><em><?php the_field('hero_description'); ?></em></p>
				</div>
			</div>
		</section>
		<section class="kidzone">
			<div class="blade">
				<div class="blade__cont">
					<h2><?php the_field('section_1_title'); ?></h2>
					<p><?php the_field('section_1_description'); ?></p>
				</div>
			</div>
			<div class="wrap-img">
				<img class="full-width-img" src="<?php echo get_template_directory_uri(); ?>/library/images/kidzone-infants.jpg" alt="Calvary Church | Connecting People with God" />
				<div class="blade">
					<div class="blade__cont">
						<h2><?php the_field('section_2_title'); ?></h2>
						<h3><?php the_field('section_2_age'); ?></h3>
						<p><?php the_field('section_2_description'); ?></p>
					</div>
				</div>
			</div>
			<div class="blade gray">
				<div class="blade__cont">
					<h2><?php the_field('section_3_title'); ?></h2>
					<h3><?php the_field('section_3_age'); ?></h3>
					<p><?php the_field('section_3_description'); ?></p>
				</div>
			</div>
			<div class="wrap-img">
				<img class="full-width-img" src="<?php echo get_template_directory_uri(); ?>/library/images/kidzone-pre-key.jpg" alt="Calvary Church | Connecting People with God" />
				<div class="blade">
					<div class="blade__cont">
						<h2><?php the_field('section_4_title'); ?></h2>
						<h3><?php the_field('section_4_age'); ?></h3>
						<p><?php the_field('section_4_description'); ?></p>
					</div>
				</div>
			</div>
			<div class="blade">
				<div class="blade__cont">
					<h2><?php the_field('section_5_title'); ?></h2>
					<h3><?php the_field('section_5_age'); ?></h3>
					<p><?php the_field('section_5_description'); ?></p>
				</div>
			</div>
			<img class="full-width-img" src="<?php echo get_template_directory_uri(); ?>/library/images/kidzone-kids.jpg" alt="Calvary Church | Connecting People with God" />
			<div class="blade gray signup--connect">
				<div class="blade__cont">
					<h2 class="title-email"><?php the_field('subscribe_title'); ?></h2>
					<p><?php the_field('subscribe_description'); ?></p>
					<div class="signup__form">
						<?php 
							$classes = get_body_class();
							if (in_array('en-US', $classes)): ?>
							<?php if ( is_active_sidebar( 'subscribe-volunteer' ) ) : ?>
								<?php dynamic_sidebar( 'subscribe-volunteer' ); ?>
							<?php endif; ?>
						<?php else : ?>
							<?php if ( is_active_sidebar( 'subscribe-voluntarios' ) ) : ?>
								<?php dynamic_sidebar( 'subscribe-voluntarios' ); ?>
							<?php endif; ?>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</section>
	</main>
<?php get_footer(); ?>
