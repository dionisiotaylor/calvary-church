<?php 
/*
	Template Name: Worship Life Calvary Church
*/
?>
<?php get_header(); ?>
	<main role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog" class="worship">
		<section class="hero hero--inner">
			<?php 
				if ( has_post_thumbnail() ) {
					the_post_thumbnail('full');
				} 
			?>
			<div class="hero__wrapper">
				<div class="hero__cont">
					<h2 class="animate"><?php the_field('hero_text'); ?> <small><?php the_field('hero_small_description'); ?> </small><span><?php the_field('hero_last_heading'); ?></span></h2>
				</div>
			</div>
		</section>
		<section class="featured">
			<h2><span><?php the_field('section_1_title'); ?></span></h2>
			<h3><?php the_field('section_1_subtitle'); ?></h3>
			<p><?php the_field('section_1_description'); ?></p>
		</section>
		
		<section class="kidzone">
			<div class="wrap-img">
				<img class="full-width-img" src="<?php the_field('1_bottom_image'); ?>" alt="Calvary Church | Connecting People with God" />
				<div class="blade">
					<div class="blade__cont">
						<?php the_field('description_section_2'); ?>
					</div>
				</div>
			</div>
			<div class="wrap-img">
				<img class="full-width-img" src="<?php the_field('2_bottom_image'); ?>" alt="Calvary Church | Connecting People with God" />
				<div class="blade">
					<div class="blade__cont">
						 <?php the_field('description_section_3'); ?>
					</div>
				</div>
			</div>
		</section>
	</main>
<?php get_footer(); ?>
