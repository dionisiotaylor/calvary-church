<!doctype html>

<!--[if lt IE 7]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--> <html <?php language_attributes(); ?> class="no-js"><!--<![endif]-->

	<head>
		<meta charset="utf-8">

		<?php // force Internet Explorer to use the latest rendering engine available ?>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">

		<title><?php wp_title(''); ?></title>

		<?php // mobile meta (hooray!) ?>
		<meta name="HandheldFriendly" content="True">
		<meta name="MobileOptimized" content="320">
		<meta name="viewport" content="width=device-width, initial-scale=1"/>

		<?php // icons & favicons (for more: http://www.jonathantneal.com/blog/understand-the-favicon/) ?>
		<link rel="apple-touch-icon" href="<?php echo get_template_directory_uri(); ?>/library/images/apple-touch-icon.png">
		<link rel="icon" href="<?php echo get_template_directory_uri(); ?>/favicon.png">
		<!--[if IE]>
			<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico">
		<![endif]-->
		<?php // or, set /favicon.ico for IE10 win ?>
		<meta name="msapplication-TileColor" content="#f01d4f">
		<meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri(); ?>/library/images/win8-tile-icon.png">
        <meta name="theme-color" content="#121212">

		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">

		<?php // wordpress head functions ?>
		<?php wp_head(); ?>
		<?php // end of wordpress head ?>

		<?php // drop Google Analytics Here ?>
		<?php // end analytics ?>
		<?php $classes = get_body_class();?>
	</head>

	<body <?php body_class(); ?> itemscope itemtype="http://schema.org/WebPage">
		<div class="close-menu"></div>
		<header class="header" role="banner" itemscope itemtype="http://schema.org/WPHeader">
			<div class="container">
				<?php 
					
					if (in_array('en-US', $classes)) {
				?>
					<h1 class="header__logo"><a href="<?php echo get_home_url('home'); ?>">Calvary Church</a></h1>
				<?php
					} else {
				?>
					<h1 class="header__logo header__logo-ES"><a href="<?php echo get_home_url('home'); ?>">Iglesia Calvario</a></h1>
				<?php	
					}
				?>
				<nav class="header__nav" role="navigation" itemscope itemtype="http://schema.org/SiteNavigationElement">
					<ul>
						<li><button class="menu" data-hook="menu">Menu</button></li>
						
						<?php if (in_array('en-US', $classes)): ?>
							<li><a href="http://live.calvaryonline.org" target="_blank" class="watch">Watch Live</a></li>
						<?php else: ?>
							<li><a href="http://live.iglesiacalvario.net/" target="_blank" class="watch">Ver En Vivo</a></li>
						<?php endif; ?>
					</ul>
				</nav>
				<nav class="header__sub-nav" role="navigation" itemscope itemtype="http://schema.org/SiteNavigationElement">
					<div class="container">
						<?php wp_nav_menu(array(
							'container' => false,
							'menu' => 'Main Navigation', 
							'menu_class' => 'single-nav',
							'theme_location' => 'know-menu'
						)); ?> 
						<?php wp_nav_menu(array(
							'container' => false,
							'menu' => 'Main Navigation', 
							'menu_class' => 'single-nav',
							'theme_location' => 'connect-menu'
						)); ?> 
						<?php wp_nav_menu(array(
							'container' => false,
							'menu' => 'Main Navigation', 
							'menu_class' => 'single-nav',
							'theme_location' => 'reach-menu'
						)); ?> 
						<?php wp_nav_menu(array(
							'container' => false,
							'menu' => 'Main Navigation', 
							'menu_class' => 'single-nav',
							'theme_location' => 'resources-menu'
						)); ?>
					</div>
					<?php //wp_nav_menu(array(
						// 'container' => false,                           // remove nav container
						// 'container_class' => 'menu cf',                 // class of container (should you choose to use it)
						// 'menu' => __( 'The Main Menu', 'bonestheme' ),  // nav name
						// 'menu_class' => 'nav top-nav cf',               // adding custom nav class
						// 'menu_id' => 'menu',							// Menu ID 
						// 'theme_location' => 'main-nav',                 // where it's located in the theme
						// 'before' => '',                                 // before the menu
						// 'after' => '',                             		// after the menu
						// 'link_before' => '',                       		// before each link
						// 'link_after' => '',                        		// after each link
						// 'depth' => 0,                              		// limit the depth of the nav
						// 'fallback_cb' => ''                             // fallback function (if there is one)
						//)); 
					?>
				</nav>
			</div>
			<!--div class="top-banner" role="banner">
				<?php if (in_array('en-US', $classes)): ?>
					<p><a href="http://calvarychurch.ticketleap.com/matt-maher/" target="_blank">2015 Matt Maher Concert. Tickets available NOW</a><span>&rarr;</span></p>
				<?php else: ?>
				<p><a href="http://calvarychurch.ticketleap.com/matt-maher/" target="_blank">2015 Matt Maher Concierto. Adquiere tu entrada AHORA</a><span>&rarr;</span></p>
				<?php endif; ?>
				<a href="" title="close" class="close">X</a>
			</div-->
		</header>
		