<?php 
/*
	Template Name: Family Night Calvary Church
*/
?>
<?php get_header(); ?>
	<main role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
		<section class="hero hero--inner">
			<?php 
				if ( has_post_thumbnail() ) {
					the_post_thumbnail('full');
				} 
			?>
			<div class="hero__wrapper">
				<div class="hero__cont">
					<h2><?php the_field('hero_title'); ?></h2>
					<p><em><?php the_field('hero_description'); ?></em></p>
				</div>
			</div>
		</section>
		<section class="kidzone">
			
			<div class="blade">
				<div class="blade__cont">
					<?php the_field('feature_quote'); ?>
				</div>
			</div>
			<div class="blade red blade__small">
				<div class="container">
					<div class="col-xs-12 col-md-4">
						<h2><?php the_field('banner_title'); ?></h2>
					</div>
					<div class="col-xs-12 col-md-8">
						<p><?php the_field('banner_description'); ?></p>
					</div>
				</div>
			</div>
			<div class="blade blade--columns">
				<span class="separator"></span>
				<div class="blade__col">
					<h2><?php the_field('row_1_left_col_title'); ?></h2>
					<?php the_field('row_1_left_column_description'); ?>
				</div>
				<div class="blade__col">
					<h2><?php the_field('row_1_right_column_title'); ?></h2>
					<?php the_field('row_1_right_column_description'); ?>
				</div>
			</div>
			<img class="full-width-img" src="<?php echo get_template_directory_uri(); ?>/library/images/family-night-kids.jpg" alt="Calvary Church | Connecting People with God" />
			<div class="blade blade--columns">
				<span class="separator"></span>
				<div class="blade__col">
					<h2><?php the_field('row_2_left_column_title'); ?></h2>
					<?php the_field('row_2_left_column_description'); ?>
				</div>
				<div class="blade__col">
					<h2><?php the_field('row_2_right_column_title'); ?></h2>
					<?php the_field('row_2_right_column_description'); ?>
				</div>
			</div>
			<img class="full-width-img" src="<?php echo get_template_directory_uri(); ?>/library/images/family-night-children-of-the-day.jpg" alt="Calvary Church | Connecting People with God" />
			<div class="blade blade--columns">
				<span class="separator"></span>
				<div class="blade__col">
					<h2><?php the_field('row_3_left_column_title'); ?></h2>
					<?php the_field('row_3_left_column_description'); ?>
				</div>
				<div class="blade__col">
					<h2><?php the_field('row_3_right_column_title'); ?></h2>
					<?php the_field('row_3_right_column_description'); ?>
				</div>
			</div>
			<img class="full-width-img" src="<?php echo get_template_directory_uri(); ?>/library/images/family-night-group.jpg" alt="Calvary Church | Connecting People with God" />

			<div class="blade blade--columns">
				<span class="separator"></span>
				<div class="blade__col">
					<h2><?php the_field('row_4_left_column_title'); ?></h2>
					<?php the_field('row_4_left_column_description'); ?>
				</div>
				<div class="blade__col">
					<h2><?php the_field('row_4_right_column_title'); ?></h2>
					<?php the_field('row_4_right_column_description'); ?>
				</div>
			</div>
			<img class="full-width-img" src="<?php echo get_template_directory_uri(); ?>/library/images/family-night-kids-workshop.jpg" alt="Calvary Church | Connecting People with God" />
			
			
			<div class="blade blade--columns">
				<span class="separator"></span>
				<div class="blade__col">
					<h2><?php the_field('row_5_left_column_title'); ?></h2>
					<?php the_field('row_5_left_column_description'); ?>
				</div>
				<div class="blade__col">
					<h2><?php the_field('row_5_right_column_title'); ?></h2>
					<?php the_field('row_5_right_column_description'); ?>
				</div>
			</div>
			<img class="full-width-img" src="<?php echo get_template_directory_uri(); ?>/library/images/family-night-womens-group.jpg" alt="Calvary Church | Connecting People with God" />
			<?php if( get_field('row_6_right_column_title') ): ?>
			<div class="blade blade--columns">
				<span class="separator"></span>
				<div class="blade__col">
					<h2><?php the_field('row_6_left_column_title'); ?></h2>
					<?php the_field('row_6_left_column_description'); ?>
				</div>
				<div class="blade__col">
					<h2><?php the_field('row_6_right_column_title'); ?></h2>
					<?php the_field('row_6_right_column_description'); ?>
				</div>
			</div>
			<?php endif;?>
			<div class="blade gray signup--connect">
				<div class="blade__cont">
					<h2 class="title-email"><?php the_field('subscribe_form_title'); ?></h2>
					<p><?php the_field('subscribe_form_description'); ?></p>
					<?php if ( is_active_sidebar( 'contact_family_night' ) ) : ?>
						<?php dynamic_sidebar( 'contact_family_night' ); ?>

					<?php else : ?>

						<div class="no-widgets">
							<p><?php _e( 'This is a widget ready area. Add some and they will appear here.', 'bonestheme' );  ?></p>
						</div>

					<?php endif; ?>
				</div>
			</div>			
		</section>
	</main>
<?php get_footer(); ?>
