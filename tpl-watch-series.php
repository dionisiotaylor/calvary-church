<?php 
/*
	Template Name: Watch Series Calvary Church
*/
?>
<?php get_header(); ?>
	<main role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
		<section class="hero">
			<?php 
				if ( has_post_thumbnail() ) {
					the_post_thumbnail('full');
				} 
			?>
			<div class="hero__wrapper">
				<div class="hero__cont">
					<h3><?php the_field('hero_title'); ?></h3>
					<img class="center" src="<?php the_field('hero_image'); ?>" alt="Calvary Church | Connecting People with God" />
					<p><?php the_field('hero_description'); ?></p>
					<p><a target="_blank" href="<?php the_field('hero_cta'); ?>" class="btn btn--ghost"><?php the_field('hero_cta_text'); ?></a></p>
				</div>
			</div>
		</section>
		<section class="kidzone">
			
			<div class="blade red blade__small">
				<ul class="tabs-links">
					<li><a href="" data-type="<?php the_field('video_type_0'); ?>"><?php the_field('video_type_0'); ?></a></li>
					<li><a href="" data-type="<?php the_field('video_type_1'); ?>"><?php the_field('video_type_1'); ?></a></li>
					<li><a href="" data-type="<?php the_field('video_type_2'); ?>"><?php the_field('video_type_2'); ?></a></li>
					<li><a href="" data-type="<?php the_field('video_type_3'); ?>"><?php the_field('video_type_3'); ?></a></li>
				</ul>
			</div>
			<div class="blade">
				<ul class="tabs-videos">
					<?php
						$args = array(
						  'post_type' => 'series'
						);
						$products = new WP_Query( $args );
						if( $products->have_posts() ) {
							while( $products->have_posts() ) {
								$products->the_post();
								if( get_field('video_combo_type') === 'Stories' || get_field('video_combo_type') === 'Creative') {
					?>
									<li data-type="<?php the_field('video_combo_type'); ?>">
										<figure>
											<img src="<?php the_field('video_image'); ?>" alt="" />
											<figcaption>
												<h2><?php the_field('video_title'); ?></h2>
												<a href="<?php the_field('video_url_from_vimeo'); ?>" class="btn btn--ghost mfp-iframe"><?php the_field('video_cta_text'); ?></a>
											</figcaption>
										</figure>
									
						<?php
								} else { ?>
									<li data-type="<?php the_field('video_combo_type'); ?>">
										<figure>
											<img src="<?php the_field('video_image'); ?>" alt="" />
											<figcaption>
												<h2><?php the_field('video_title'); ?></h2>
												<a href="<?php the_field('video_cta'); ?>" class="btn btn--ghost"><?php the_field('video_cta_text'); ?></a>
											</figcaption>
										</figure>
									
								<?php
								}
							}
						}
						else { echo 'Oh no series at the time!'; }
						wp_reset_query();
					?>
				</ul>
			</div>
		</section>
	</main>
<?php get_footer(); ?>
