<?php get_header(); ?>

	<div class="video-wrapper">
		<video autoplay muted loop>
			<source src="<?php echo get_template_directory_uri(); ?>/library/videos/welcome-video.webm" type="video/webm" />
			<source src="<?php echo get_template_directory_uri(); ?>/library/videos/welcome-video.mp4" type="video/mp4" />
			<source src="<?php echo get_template_directory_uri(); ?>/library/videos/welcome-video.ogv" type="video/ogg" />
		</video>
	</div>
	<section class="center-container welcome">
		<h2>Welcome<span>Bienvenidos</span></h2>
		<p>
			<a href="<?php echo get_permalink(get_page_by_path('home')); ?>" class="btn btn--ghost">English</a>
			<a href="<?php echo get_permalink(get_page_by_path('inicio')); ?>" class="btn btn--ghost">Espa&nacute;ol</a>
		</p>
		<img class="logo" src="<?php echo get_template_directory_uri(); ?>/library/images/logo.png" alt="" />
</section>


	
<?php get_footer(); ?>
