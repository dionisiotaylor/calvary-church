		<footer class="footer" role="contentinfo" itemscope itemtype="http://schema.org/WPFooter">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-sm-4 col-md-4">
						<?php 
							$classes = get_body_class();
							if (in_array('en-US', $classes)) {
						?>
							<a href="<?php echo get_page_link(get_page_by_path('contact-us')); ?>" class="contact-us">Contact us</a>
						<?php
							} else {
						?>
							<a href="<?php echo get_page_link(get_page_by_path('contactanos')); ?>" class="contact-us">Contáctanos</a>
						<?php }?>
					</div>
					<div class="col-xs-12 col-sm-4 col-md-4">
						<?php if(in_array('en-US', $classes)): ?>
							<ul>
								<li>501 W. Bypass 61, Muscatine, Iowa.</li>
								<li>Phone: 563-263-3367</li>
								<li>Email: <a href="mailto:info@calvaryonline.org">info@calvaryonline.org</a></li>
							</ul>
						<?php else: ?>
							<ul>
								<li>501 W. Bypass 61, Muscatine, Iowa.</li>
								<li>Teléfono: 563-263-3367</li>
								<li>Correo: <a href="mailto:info@calvaryonline.org">info@calvaryonline.org</a></li>
							</ul>
						<?php endif;?>
					</div>
					<div class="col-xs-12 col-sm-4 col-md-4">
						<ul class="social">
							<li>
								
								<?php 
									$classes = get_body_class();
									if (in_array('en-US', $classes)) {
								?>
									<a class="facebook" href="https://www.facebook.com/calvarymuscatine" target="_blank">
										<svg viewBox="0 0 512 512">
											<path d="M211.9 197.4h-36.7v59.9h36.7V433.1h70.5V256.5h49.2l5.2-59.1h-54.4c0 0 0-22.1 0-33.7 0-13.9 2.8-19.5 16.3-19.5 10.9 0 38.2 0 38.2 0V82.9c0 0-40.2 0-48.8 0 -52.5 0-76.1 23.1-76.1 67.3C211.9 188.8 211.9 197.4 211.9 197.4z"></path>
										</svg>
									</a>
								<?php
									} else {
								?>
									<a class="facebook" href="https://www.facebook.com/IglesiaCalvarioMuscatine" target="_blank">
										<svg viewBox="0 0 512 512">
											<path d="M211.9 197.4h-36.7v59.9h36.7V433.1h70.5V256.5h49.2l5.2-59.1h-54.4c0 0 0-22.1 0-33.7 0-13.9 2.8-19.5 16.3-19.5 10.9 0 38.2 0 38.2 0V82.9c0 0-40.2 0-48.8 0 -52.5 0-76.1 23.1-76.1 67.3C211.9 188.8 211.9 197.4 211.9 197.4z"></path>
										</svg>
									</a>
								<?php	
									}
								?>
									
							</li>
							<li>
								<a class="youtube" href="https://vimeo.com/calvarymuscatine" target="_blank">
									<svg viewBox="0 0 512 512"><path d="M420.1 197.9c-1.5 33.6-25 79.5-70.3 137.8 -46.9 60.9-86.5 91.4-118.9 91.4 -20.1 0-37.1-18.5-51-55.6 -9.3-34-18.5-68-27.8-102 -10.3-37.1-21.4-55.7-33.2-55.7 -2.6 0-11.6 5.4-27 16.2L75.7 209.1c17-14.9 33.8-29.9 50.3-44.9 22.7-19.6 39.7-29.9 51.1-31 26.8-2.6 43.3 15.8 49.5 55 6.7 42.4 11.3 68.7 13.9 79 7.7 35.1 16.2 52.7 25.5 52.7 7.2 0 18-11.4 32.5-34.2 14.4-22.8 22.2-40.1 23.2-52.1 2.1-19.7-5.7-29.5-23.2-29.5 -8.3 0-16.8 1.9-25.5 5.7 16.9-55.5 49.3-82.4 97.1-80.9C405.5 130 422.2 153 420.1 197.9z"/></svg>
								</a>
							</li>
							<li>
								<a class="email" href="mailto:info@calvaryonline.org">
									<svg viewBox="0 0 512 512"><path d="M101.3 141.6v228.9h0.3 308.4 0.8V141.6H101.3zM375.7 167.8l-119.7 91.5 -119.6-91.5H375.7zM127.6 194.1l64.1 49.1 -64.1 64.1V194.1zM127.8 344.2l84.9-84.9 43.2 33.1 43-32.9 84.7 84.7L127.8 344.2 127.8 344.2zM384.4 307.8l-64.4-64.4 64.4-49.3V307.8z"/></svg>
								</a>
							</li>
						</ul>
						<?php if(in_array('en-US', $classes)): ?>
							<p class="copyright">Copyright &copy; <?php echo date('Y'); ?> Calvary Church. All rights reserved. </p>
						<?php else: ?>
							<p class="copyright">Copyright &copy; <?php echo date('Y'); ?> Iglesia Calvario. Derechos Reservados. </p>
						<?php endif;?>
					</div>
				</div>
			</div>
			
		</footer>

		<?php // all js scripts are loaded in library/bones.php ?>
		<?php wp_footer(); ?>
		<?php if ( is_page("contact-us") || is_page("contactanos") ) : ?>
			<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC32-wV9zxU0KprIrpWuaw0T75wLus2cGU"></script>
			<script type="text/javascript">
				function init() {
					var myLatlng = new google.maps.LatLng(41.454489, -91.035858);
					var mapOptions = {
					  center: new google.maps.LatLng(41.454489, -91.035858),
					  zoom: 17,
					  mapTypeId: google.maps.MapTypeId.ROADMAP
					};
					var map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);
					var marker = new google.maps.Marker({
						  position: myLatlng,
						  map: map,
						  title:"Hello World!",
						  icon:"http://www.dionisiotaylor.com/calvary-ping.png"
					});

					google.maps.event.addListener(map, 'click', function(e) {
						console.log(e);
					});
				}
				google.maps.event.addDomListener(window, 'load', init);
			</script>
		<?php endif; ?>
	</body>

</html> <!-- end of site. what a ride! -->
