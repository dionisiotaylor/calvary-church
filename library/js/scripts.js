/*
 * Bones Scripts File
 * Author: Eddie Machado
 *
 * This file should contain any js scripts you want to add to the site.
 * Instead of calling it in the header or throwing it inside wp_head()
 * this file will be called automatically in the footer so as not to
 * slow the page load.
 *
 * There are a lot of example functions and tools in here. If you don't
 * need any of it, just remove it. They are meant to be helpers and are
 * not required. It's your world baby, you can do whatever you want.
*/


/*
 * Get Viewport Dimensions
 * returns object with viewport dimensions to match css in width and height properties
 * ( source: http://andylangton.co.uk/blog/development/get-viewport-size-width-and-height-javascript )
*/
function updateViewportDimensions() {
	var w=window,d=document,e=d.documentElement,g=d.getElementsByTagName('body')[0],x=w.innerWidth||e.clientWidth||g.clientWidth,y=w.innerHeight||e.clientHeight||g.clientHeight;
	return { width:x,height:y };
}
// setting the viewport width
var viewport = updateViewportDimensions();


/*
 * Throttle Resize-triggered Events
 * Wrap your actions in this function to throttle the frequency of firing them off, for better performance, esp. on mobile.
 * ( source: http://stackoverflow.com/questions/2854407/javascript-jquery-window-resize-how-to-fire-after-the-resize-is-completed )
*/
var waitForFinalEvent = (function () {
	var timers = {};
	return function (callback, ms, uniqueId) {
		if (!uniqueId) { uniqueId = "Don't call this twice without a uniqueId"; }
		if (timers[uniqueId]) { clearTimeout (timers[uniqueId]); }
		timers[uniqueId] = setTimeout(callback, ms);
	};
})();

// how long to wait before deciding the resize has stopped, in ms. Around 50-100 should work ok.
var timeToWaitForLast = 100;


/*
 * Here's an example so you can see how we're using the above function
 *
 * This is commented out so it won't work, but you can copy it and
 * remove the comments.
 *
 *
 *
 * If we want to only do it on a certain page, we can setup checks so we do it
 * as efficient as possible.
 *
 * if( typeof is_home === "undefined" ) var is_home = $('body').hasClass('home');
 *
 * This once checks to see if you're on the home page based on the body class
 * We can then use that check to perform actions on the home page only
 *
 * When the window is resized, we perform this function
 * $(window).resize(function () {
 *
 *    // if we're on the home page, we wait the set amount (in function above) then fire the function
 *    if( is_home ) { waitForFinalEvent( function() {
 *
 *	// update the viewport, in case the window size has changed
 *	viewport = updateViewportDimensions();
 *
 *      // if we're above or equal to 768 fire this off
 *      if( viewport.width >= 768 ) {
 *        console.log('On home page and window sized to 768 width or more.');
 *      } else {
 *        // otherwise, let's do this instead
 *        console.log('Not on home page, or window sized to less than 768.');
 *      }
 *
 *    }, timeToWaitForLast, "your-function-identifier-string"); }
 * });
 *
 * Pretty cool huh? You can create functions like this to conditionally load
 * content and other stuff dependent on the viewport.
 * Remember that mobile devices and javascript aren't the best of friends.
 * Keep it light and always make sure the larger viewports are doing the heavy lifting.
 *
*/

/*
 * We're going to swap out the gravatars.
 * In the functions.php file, you can see we're not loading the gravatar
 * images on mobile to save bandwidth. Once we hit an acceptable viewport
 * then we can swap out those images since they are located in a data attribute.
*/
function loadGravatars() {
  // set the viewport using the function above
  viewport = updateViewportDimensions();
  // if the viewport is tablet or larger, we load in the gravatars
  if (viewport.width >= 768) {
  jQuery('.comment img[data-gravatar]').each(function(){
    jQuery(this).attr('src',jQuery(this).attr('data-gravatar'));
  });
	}
} // end function


/*
 * Put all your regular jQuery in here.
*/
(function($,sr){

  // debouncing function from John Hann
  // http://unscriptable.com/index.php/2009/03/20/debouncing-javascript-methods/
  var debounce = function (func, threshold, execAsap) {
	  var timeout;

	  return function debounced () {
		  var obj = this, args = arguments;
		  function delayed () {
			  if (!execAsap)
				  func.apply(obj, args);
			  timeout = null;
		  }

		  if (timeout)
			  clearTimeout(timeout);
		  else if (execAsap)
			  func.apply(obj, args);

		  timeout = setTimeout(delayed, threshold || 500);
	  };
  };
  // smartresize 
  jQuery.fn[sr] = function(fn){  return fn ? this.bind('resize', debounce(fn)) : this.trigger(sr); };

})(jQuery,'smartresize');


 
jQuery(document).ready(function() {
  
	jQuery('.blade.events > div').slick({
		arrows: true,
		slidesToShow: 1
	});
	
	// Scroll Animation
	jQuery('.btn-scroll[href*=#]:not([href=#])').on('click', function() {
		if (location.pathname.replace(/^\//,'') === this.pathname.replace(/^\//,'') && location.hostname === this.hostname) {
		  var target = jQuery(this.hash);
		  target = target.length ? target : jQuery('[name=' + this.hash.slice(1) +']');
		  if (target.length) {
			jQuery('html,body').animate({
			  scrollTop: target.offset().top
			}, 1500);
			return false;
		  }
		}
	});
	
	jQuery('.hero__slider').slick({
		dots: false,
		autoplay: true,
		autoplaySpeed: 6000,
		pauseOnHover: false,
		arrows: true,
		infinite: true,
		speed: 500,
		fade: true,
		cssEase: 'linear'
	});
	
	
	// Video stuff
	jQuery('.feature-video .play').on('click', function() {
		jQuery('.feature-video img').fadeOut();
		jQuery(this).fadeOut();
		jQuery('.feature-video video')[0].play();
	});
	jQuery('.feature-video video').on('ended',function() {
		jQuery('.feature-video .play').fadeIn();
		jQuery('.feature-video img').fadeIn();
	});
	jQuery('.banner__video .play').on('click',function() {
		jQuery('.banner__video .play').fadeOut();
		jQuery('#video-ty').animate({'opacity': 1});
		jQuery('#video-ty')[0].play();
	});
	jQuery('#video-ty').on('ended',function() {
		jQuery('.banner__video .play').fadeIn();
		jQuery(this).animate({'opacity': 0});
	});
	
	
	// Toggle Menu
	jQuery('.header__nav button[data-hook="menu"]').on('click', function(e) {
		e.preventDefault();
		jQuery('body').toggleClass('scroll-lock');
		
		jQuery('.header__sub-nav').slideToggle();
		jQuery('.close-menu').toggle();
	});
	jQuery('.close-menu').on('click', function() {
		jQuery('body').toggleClass('scroll-lock');
		jQuery('.header__sub-nav').slideUp();
		jQuery(this).hide();
	});
	jQuery('.single-nav > li > a').on('click', function(e) {
		e.preventDefault();
	});
	
	
	jQuery('.volunteer-info a').on('click', function(e) {
		var self = this;
		e.preventDefault();
		
		jQuery(this).parents('li').find('.volunteer-info').fadeOut();
		jQuery(this).parents('li').find('.volunteer-form').fadeIn();
	});
	
	jQuery('.volunteer-form a[data-hook="back"]').on('click',function(e) {
		e.preventDefault();
		jQuery(this).parents('li').find('.volunteer-info').fadeIn();
		jQuery(this).parents('li').find('.volunteer-form').fadeOut();
	});
	
//	setTimeout(function() {
//		jQuery('.hero__cont h2').addClass('animate');
//	},1000);
//	$objWindow = jQuery(window), jQuery("div.heading-top").each(function() {
//		var a = jQuery(this);
//		jQuery(window).scroll(function() {
//			var b = -($objWindow.scrollTop() / 10),
//				c = "center " + 8 * b + "px";
//			a.css({
//				'top': c
//			});
//		});
//	});
	jQuery('.header__sub-nav').delegate('.mobile-menu > li','click', function(e) {
		
		if( jQuery(this).find('.sub-menu').is(":visible") ) {
			jQuery('.sub-menu', this).slideToggle();
		} else  {
			jQuery('.mobile-menu > li').not(this).find('> ul').slideUp();
			jQuery('.sub-menu', this).slideToggle();
		}
	});
	
	
	
	var baseURL = jQuery('.header__logo a').attr('href');
	
	if(window.location.href.indexOf("/en/") > -1) {   
		jQuery('.header__logo a').attr('href', baseURL + '/home/');
    } else {
		jQuery('.header__logo a').attr('href', baseURL + '/inicio/');
	}
	

	// Filter videos
	jQuery('.tabs-links a').on('click', function(e) {
		e.preventDefault();
		var filterType = jQuery(this).attr('data-type');
		var videoType  = jQuery('.tabs-videos li').attr('data-type');
		jQuery('.tabs-videos li').not('[data-type="'+ filterType + '"]').hide();
		jQuery('.tabs-videos li[data-type="'+ filterType + '"]').show();
		if(filterType === 'all' || filterType === 'todos') {
			jQuery('.tabs-videos li').show();
		}
	});

	
	// POP UP
	jQuery('.mfp-iframe').magnificPopup({
		type: 'inline'
	});
	
	jQuery('.mfp-image').magnificPopup({type:'image'});
	function resizeWindow() {
		
		if (jQuery(window).width() <= 768) {
			jQuery('.single-nav').addClass('mobile-menu');
			jQuery('.header__sub-nav').css('bottom',0);
		} else {
			jQuery('.single-nav').removeClass('mobile-menu');
			jQuery('.sub-menu').hide();
			jQuery('.header__sub-nav').css('bottom','initial');
		}
		// Center hero image
//		jQuery(".hero > img").each(function(i, img) {
//			jQuery(img).css({
//				left: (jQuery(img).parent().width()/2) - (jQuery(img).width()/2)
//			});
//		});
	}
	
	
	// Local Storage for special Event banner at the top of the page
	var current   = sessionStorage.getItem('topBanner'); 
	var topBanner = sessionStorage.setItem('topBanner', current);
	setTimeout(function() {
		jQuery('.top-banner').animate({'top':'42px'});
	},2000);
	jQuery('.top-banner .close').on('click', function(e) {
		e.preventDefault();
		jQuery('.top-banner').animate({'top':'-100px'});
		topBanner = sessionStorage.setItem('topBanner', 'false');
	});
	if(current === 'false') {
		jQuery('.top-banner').hide();
	}
	

	resizeWindow();
//	jQuery(window).smartresize(resizeWindow);
	jQuery(window).resize(function() {
		waitForFinalEvent(resizeWindow, 500, "some unique string");
		console.log('resize');
	});
	
	
}); /* end of as page load scripts */
