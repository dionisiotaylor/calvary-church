<?php 
/*
	Template Name: Watch Series Single Calvary Church
*/
?>
<?php get_header(); ?>
	<main role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
		<section class="hero">
			<?php 
				if ( has_post_thumbnail() ) {
					the_post_thumbnail('full');
				} 
			?>
		</section>
		<section class="kidzone">
			<div class="blade">
			
				<ul class="list-single-series">
					<?php if ( get_field('1_title') ) : ?>
						<li>
							<div class="container">
								<div class="col-xs-12 col-sm-6 col-md-5 col-md-offset-1">
									<h2>1</h2>
									<ul>
										<li><?php the_field('1_date'); ?></li>
										<li><?php the_field('1_title'); ?></li>
										<li><?php the_field('1_pastor'); ?></li>
										<li><p><?php the_field('1_description'); ?></p></li>
									</ul>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-5">
									<p class="controls">
										<a href="<?php the_field('1_video'); ?>" class="mfp-iframe btn btn--ghost"><?php the_field('1_video_cta'); ?></a>
										<?php if ( get_field('1_mp3') ) : ?>
											<a href="<?php the_field('1_mp3_cta'); ?>" class="mfp-iframe btn btn--ghost"><?php the_field('1_mp3'); ?></a>
										<?php endif; ?>
									</p>
								</div>
							</div>
						</li>
					<?php endif; ?>
					
					<?php if ( get_field('2_date') ) : ?>
						<li>
							<div class="container">
								<div class="col-xs-12 col-sm-6 col-md-5 col-md-offset-1">
									<h2>2</h2>
									<ul>
										<li><?php the_field('2_date'); ?></li>
										<li><?php the_field('2_title'); ?></li>
										<li><?php the_field('2_pastor'); ?></li>
										<li><p><?php the_field('2_description'); ?></p></li>
									</ul>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-5">
									<p class="controls">
										<a href="<?php the_field('2_video'); ?>" class="mfp-iframe btn btn--ghost"><?php the_field('2_video_cta'); ?></a>
										<?php if ( get_field('2_mp3') ) : ?>
											<a href="<?php the_field('2_mp3_cta'); ?>" class="mfp-iframe btn btn--ghost"><?php the_field('2_mp3'); ?></a>
										<?php endif; ?>
									</p>
								</div>
							</div>
						</li>
					<?php endif; ?>
					
					<?php if ( get_field('3_title') ) : ?>
						<li>
							<div class="container">
								<div class="col-xs-12 col-sm-6 col-md-5 col-md-offset-1">
									<h2>3</h2>
									<ul>
										<li><?php the_field('3_date'); ?></li>
										<li><?php the_field('3_title'); ?></li>
										<li><?php the_field('3_pastor'); ?></li>
										<li><p><?php the_field('3_description'); ?></p></li>
									</ul>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-5">
									<p class="controls">
										<a href="<?php the_field('3_video'); ?>" class="mfp-iframe btn btn--ghost"><?php the_field('3_video_cta'); ?></a>
										<?php if ( get_field('3_mp3') ) : ?>
											<a href="<?php the_field('3_mp3_cta'); ?>" class="mfp-iframe btn btn--ghost"><?php the_field('3_mp3'); ?></a>
										<?php endif; ?>
									</p>
								</div>
							</div>
						</li>
					<?php endif; ?>
					
					<?php if ( get_field('4_title') ) : ?>
						<li>
							<div class="container">
								<div class="col-xs-12 col-sm-6 col-md-5 col-md-offset-1">
									<h2>4</h2>
									<ul>
										<li><?php the_field('4_date'); ?></li>
										<li><?php the_field('4_title'); ?></li>
										<li><?php the_field('4_pastor'); ?></li>
										<li><p><?php the_field('4_description'); ?></p></li>
									</ul>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-5">
									<p class="controls">
										<a href="<?php the_field('4_video'); ?>" class="mfp-iframe btn btn--ghost"><?php the_field('4_video_cta'); ?></a>
										<?php if ( get_field('4_mp3') ) : ?>
											<a href="<?php the_field('4_mp3_cta'); ?>" class="mfp-iframe btn btn--ghost"><?php the_field('4_mp3'); ?></a>
										<?php endif; ?>
									</p>
								</div>
							</div>
						</li>
					<?php endif; ?>
					
					<?php if ( get_field('5_title') ) : ?>
						<li>
							<div class="container">
								<div class="col-xs-12 col-sm-6 col-md-5 col-md-offset-1">
									<h2>5</h2>
									<ul>
										<li><?php the_field('5_date'); ?></li>
										<li><?php the_field('5_title'); ?></li>
										<li><?php the_field('5_pastor'); ?></li>
										<li><p><?php the_field('5_description'); ?></p></li>
									</ul>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-5">
									<p class="controls">
										<a href="<?php the_field('5_video'); ?>" class="mfp-iframe btn btn--ghost"><?php the_field('5_video_cta'); ?></a>
										<?php if ( get_field('5_mp3') ) : ?>
											<a href="<?php the_field('5_mp3_cta'); ?>" class="mfp-iframe btn btn--ghost"><?php the_field('1_mp3'); ?></a>
										<?php endif; ?>
									</p>
								</div>
							</div>
						</li>
					<?php endif; ?>
					
					<?php if ( get_field('6_title') ) : ?>
						<li>
							<div class="container">
								<div class="col-xs-12 col-sm-6 col-md-5 col-md-offset-1">
									<h2>6</h2>
									<ul>
										<li><?php the_field('6_date'); ?></li>
										<li><?php the_field('6_title'); ?></li>
										<li><?php the_field('6_pastor'); ?></li>
										<li><p><?php the_field('6_description'); ?></p></li>
									</ul>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-5">
									<p class="controls">
										<a href="<?php the_field('6_video'); ?>" class="mfp-iframe btn btn--ghost"><?php the_field('6_video_cta'); ?></a>
										<?php if ( get_field('6_mp3') ) : ?>
											<a href="<?php the_field('6_mp3_cta'); ?>" class="mfp-iframe btn btn--ghost"><?php the_field('6_mp3'); ?></a>
										<?php endif; ?>
									</p>
								</div>
							</div>
						</li>
					<?php endif; ?>
					
					<?php if ( get_field('7_title') ) : ?>
						<li>
							<div class="container">
								<div class="col-xs-12 col-sm-6 col-md-5 col-md-offset-1">
									<h2>7</h2>
									<ul>
										<li><?php the_field('7_date'); ?></li>
										<li><?php the_field('7_title'); ?></li>
										<li><?php the_field('7_pastor'); ?></li>
										<li><p><?php the_field('7_description'); ?></p></li>
									</ul>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-5">
									<p class="controls">
										<a href="<?php the_field('7_video'); ?>" class="mfp-iframe btn btn--ghost"><?php the_field('7_video_cta'); ?></a>
										<?php if ( get_field('7_mp3') ) : ?>
											<a href="<?php the_field('7_mp3_cta'); ?>" class="mfp-iframe btn btn--ghost"><?php the_field('7_mp3'); ?></a>
										<?php endif; ?>
									</p>
								</div>
							</div>
						</li>
					<?php endif; ?>
					
					<?php if ( get_field('8_title') ) : ?>
						<li>
							<div class="container">
								<div class="col-xs-12 col-sm-6 col-md-5 col-md-offset-1">
									<h2>8</h2>
									<ul>
										<li><?php the_field('8_date'); ?></li>
										<li><?php the_field('8_title'); ?></li>
										<li><?php the_field('8_pastor'); ?></li>
										<li><p><?php the_field('8_description'); ?></p></li>
									</ul>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-5">
									<p class="controls">
										<a href="<?php the_field('8_video'); ?>" class="mfp-iframe btn btn--ghost"><?php the_field('8_video_cta'); ?></a>
										<?php if ( get_field('8_mp3') ) : ?>
											<a href="<?php the_field('8_mp3_cta'); ?>" class="mfp-iframe btn btn--ghost"><?php the_field('8_mp3'); ?></a>
										<?php endif; ?>
									</p>
								</div>
							</div>
						</li>
					<?php endif; ?>
					
					<?php if ( get_field('9_title') ) : ?>
						<li>
							<div class="container">
								<div class="col-xs-12 col-sm-6 col-md-5 col-md-offset-1">
									<h2>9</h2>
									<ul>
										<li><?php the_field('9_date'); ?></li>
										<li><?php the_field('9_title'); ?></li>
										<li><?php the_field('9_pastor'); ?></li>
										<li><p><?php the_field('9_description'); ?></p></li>
									</ul>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-5">
									<p class="controls">
										<a href="<?php the_field('9_video'); ?>" class="mfp-iframe btn btn--ghost"><?php the_field('9_video_cta'); ?></a>
										<?php if ( get_field('9_mp3') ) : ?>
											<a href="<?php the_field('9_mp3_cta'); ?>" class="mfp-iframe btn btn--ghost"><?php the_field('9_mp3'); ?></a>
										<?php endif; ?>
									</p>
								</div>
							</div>
						</li>
					<?php endif; ?>
					
					<?php if ( get_field('10_title') ) : ?>
						<li>
							<div class="container">
								<div class="col-xs-12 col-sm-6 col-md-5 col-md-offset-1">
									<h2>10</h2>
									<ul>
										<li><?php the_field('10_date'); ?></li>
										<li><?php the_field('10_title'); ?></li>
										<li><?php the_field('10_pastor'); ?></li>
										<li><p><?php the_field('10_description'); ?></p></li>
									</ul>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-5">
									<p class="controls">
										<a href="<?php the_field('10_video'); ?>" class="mfp-iframe btn btn--ghost"><?php the_field('10_video_cta'); ?></a>
										<?php if ( get_field('10_mp3') ) : ?>
											<a href="<?php the_field('10_mp3_cta'); ?>" class="mfp-iframe btn btn--ghost"><?php the_field('10_mp3'); ?></a>
										<?php endif; ?>
									</p>
								</div>
							</div>
						</li>
					<?php endif; ?>
					
					<?php if ( get_field('11_title') ) : ?>
						<li>
							<div class="container">
								<div class="col-xs-12 col-sm-6 col-md-5 col-md-offset-1">
									<h2>11</h2>
									<ul>
										<li><?php the_field('11_date'); ?></li>
										<li><?php the_field('11_title'); ?></li>
										<li><?php the_field('11_pastor'); ?></li>
										<li><p><?php the_field('11_description'); ?></p></li>
									</ul>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-5">
									<p class="controls">
										<a href="<?php the_field('11_video'); ?>" class="mfp-iframe btn btn--ghost"><?php the_field('11_video_cta'); ?></a>
										<?php if ( get_field('11_mp3') ) : ?>
											<a href="<?php the_field('11_mp3_cta'); ?>" class="mfp-iframe btn btn--ghost"><?php the_field('11_mp3'); ?></a>
										<?php endif; ?>
									</p>
								</div>
							</div>
						</li>
					<?php endif; ?>
					
					<?php if ( get_field('12_title') ) : ?>
						<li>
							<div class="container">
								<div class="col-xs-12 col-sm-6 col-md-5 col-md-offset-1">
									<h2>12</h2>
									<ul>
										<li><?php the_field('12_date'); ?></li>
										<li><?php the_field('12_title'); ?></li>
										<li><?php the_field('12_pastor'); ?></li>
										<li><p><?php the_field('12_description'); ?></p></li>
									</ul>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-5">
									<p class="controls">
										<a href="<?php the_field('12_video'); ?>" class="mfp-iframe btn btn--ghost"><?php the_field('12_video_cta'); ?></a>
										<?php if ( get_field('12_mp3') ) : ?>
											<a href="<?php the_field('12_mp3_cta'); ?>" class="mfp-iframe btn btn--ghost"><?php the_field('12_mp3'); ?></a>
										<?php endif; ?>
									</p>
								</div>
							</div>
						</li>
					<?php endif; ?>
					
					<?php if ( get_field('13_title') ) : ?>
						<li>
							<div class="container">
								<div class="col-xs-12 col-sm-6 col-md-5 col-md-offset-1">
									<h2>13</h2>
									<ul>
										<li><?php the_field('13_date'); ?></li>
										<li><?php the_field('13_title'); ?></li>
										<li><?php the_field('13_pastor'); ?></li>
										<li><p><?php the_field('13_description'); ?></p></li>
									</ul>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-5">
									<p class="controls">
										<a href="<?php the_field('13_video'); ?>" class="mfp-iframe btn btn--ghost"><?php the_field('13_video_cta'); ?></a>
										<?php if ( get_field('13_mp3') ) : ?>
											<a href="<?php the_field('13_mp3_cta'); ?>" class="mfp-iframe btn btn--ghost"><?php the_field('13_mp3'); ?></a>
										<?php endif; ?>
									</p>
								</div>
							</div>
						</li>
					<?php endif; ?>
					
					<?php if ( get_field('14_title') ) : ?>
						<li>
							<div class="container">
								<div class="col-xs-12 col-sm-6 col-md-5 col-md-offset-1">
									<h2>14</h2>
									<ul>
										<li><?php the_field('14_date'); ?></li>
										<li><?php the_field('14_title'); ?></li>
										<li><?php the_field('14_pastor'); ?></li>
										<li><p><?php the_field('14_description'); ?></p></li>
									</ul>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-5">
									<p class="controls">
										<a href="<?php the_field('14_video'); ?>" class="mfp-iframe btn btn--ghost"><?php the_field('14_video_cta'); ?></a>
										<?php if ( get_field('14_mp3') ) : ?>
											<a href="<?php the_field('14_mp3_cta'); ?>" class="mfp-iframe btn btn--ghost"><?php the_field('14_mp3'); ?></a>
										<?php endif; ?>
									</p>
								</div>
							</div>
						</li>
					<?php endif; ?>
				</ul>
			</div>
		</section>
	</main>
<?php get_footer(); ?>
