<?php 
/*
	Template Name: Axis Calvary Church
*/
?>
<?php get_header(); ?>
	<main role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
		<section class="hero">
			<?php 
				if ( has_post_thumbnail() ) {
					the_post_thumbnail('full');
				} 
			?>
			<div class="hero__wrapper">
				<div class="hero__cont">
					<h2><?php the_field('hero_title'); ?></h2>
					<p><em><?php the_field('hero_description'); ?></em></p>
				</div>
			</div>
		</section>
		<section class="values">
			<div class="blade">
				<div class="container">
					<div class="col-xs-12 col-sm-5 col-md-3 col-md-offset-2">
						<h2 class="title-value"><?php the_field('value_title_1'); ?></h2>
					</div>
					<div class="col-xs-12 col-sm-7 col-md-7">
						<div class="center">
							<div class="values__cont">
								<p><?php the_field('value_description_1'); ?></p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="blade grow">
				<div class="container">
					<div class="col-xs-12 col-sm-5 col-md-5 right">
						<h2 class="title-value"><?php the_field('value_title_2'); ?></h2>
					</div>
					<div class="col-xs-12 col-sm-7 col-md-5 col-md-offset-2">
						<div class="center">
							<div class="values__cont">
								<p><?php the_field('value_description_2'); ?></p>
							</div>
						</div>
					</div>
					
				</div>
			</div>
			
			<div class="blade">
				<div class="container">
					<div class="col-xs-12 col-sm-5 col-md-3 col-md-offset-2">
						<h2 class="title-value"><?php the_field('value_title_3'); ?></h2>
					</div>
					<div class="col-xs-12 col-sm-7 col-md-7">
						<div class="center">
							<div class="values__cont">
								<p><?php the_field('value_description_3'); ?></p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="blade">
				<div class="container">
					<div class="col-xs-12 col-sm-5 col-md-3 col-md-offset-2">
						<h2 class="title-value"><?php the_field('value_title_4'); ?></h2>
					</div>
					<div class="col-xs-12 col-sm-7 col-md-7">
						<div class="center">
							<div class="values__cont">
								<p><?php the_field('value_description_4'); ?></p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	</main>
<?php get_footer(); ?>
