module.exports = {

	"greensock": {
		exports: "TweenMax"
	},

	"handlebars": {
		exports: "Handlebars"
	},

	"underscore": {
		exports: "_"
	},

	"backbone": {
		exports: "Backbone",
		depends: {
			jquery: "$",
			underscore: "_"
		}
	},

	"backbone.super": {
		exports: "Backbone",
		depends: {
			backbone: "Backbone"
		}
	},

	"jquery": {
		exports: "$"
	}

};