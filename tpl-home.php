<?php 
/*
	Template Name: Home Calvary Church
*/
$classes = get_body_class();
?>
<?php get_header(); ?>
	<main role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
		<section class="hero">
			<div class="hero__slider">
				
				<?php if ( get_field('1_hero_image') ) : ?>
					<div class="slide">
						<img src="<?php the_field('1_hero_image'); ?>" class="full-width-img" alt="" />
						<?php if ( get_field('1_hero_cta') ) : ?>
							<a href="<?php the_field('1_hero_cta_link'); ?>" class="hero__cta"><?php the_field('1_hero_cta'); ?></a>
						<?php endif;?>
						<div class="hero__wrapper">
							<div class="hero__cont">
								<?php if ( get_field('1_hero_text') ) : ?>
									<h2><?php the_field('1_hero_text'); ?></h2>
								<?php endif;?>
							</div>
						</div>
					</div><!-- slide -->
				<?php endif; ?>
				
				<?php if ( get_field('2_hero_image') ) : ?>
					<div class="slide">
						<img src="<?php the_field('2_hero_image'); ?>" class="full-width-img" alt="" />
						<?php if ( get_field('2_hero_cta') ) : ?>
							<a href="<?php the_field('2_hero_cta_link'); ?>" class="hero__cta"><?php the_field('2_hero_cta'); ?></a>
						<?php endif;?>
						<div class="hero__wrapper">
							<div class="hero__cont">
								<?php if ( get_field('2_hero_text') ) : ?>
									<h2><?php the_field('2_hero_text'); ?></h2>
								<?php endif;?>
							</div>
						</div>
					</div><!-- slide -->
				<?php endif; ?>
				
				<?php if ( get_field('3_hero_image') ) : ?>
					<div class="slide">
						<img src="<?php the_field('3_hero_image'); ?>" class="full-width-img" alt="" />
						<?php if ( get_field('3_hero_cta') ) : ?>
							<a href="<?php the_field('3_hero_cta_link'); ?>" class="hero__cta"><?php the_field('3_hero_cta'); ?></a>
						<?php endif;?>
						<div class="hero__wrapper">
							<div class="hero__cont">
								<?php if ( get_field('3_hero_text') ) : ?>
									<h2><?php the_field('3_hero_text'); ?></h2>
								<?php endif;?>
							</div>
						</div>
					</div><!-- slide -->
				<?php endif; ?>
				
				<?php if ( get_field('4_hero_image') ) : ?>
					<div class="slide">
						<img src="<?php the_field('4_hero_image'); ?>" class="full-width-img" alt="" />
						<?php if ( get_field('4_hero_cta') ) : ?>
							<a href="<?php the_field('4_hero_cta_link'); ?>" class="hero__cta"><?php the_field('4_hero_cta'); ?></a>
						<?php endif;?>
						<div class="hero__wrapper">
							<div class="hero__cont">
								<?php if ( get_field('4_hero_text') ) : ?>
									<h2><?php the_field('4_hero_text'); ?></h2>
								<?php endif;?>
							</div>
						</div>
					</div><!-- slide -->
				<?php endif; ?>
				
			</div>
		</section>
		<section id="services" class="banner banner--services">
			<div class="container">
				<div class="banner__cont">
					<h2><?php the_field('intro_video_title'); ?></h2>
					<h3><?php the_field('intro_video_subtitle'); ?></h3>
					<p><?php the_field('intro_video_services_time'); ?></p>
				</div>
			</div>
		</section>
		<section class="blade feature-video">
			<video autoplay muted loop id="video2" poster="<?php echo get_template_directory_uri(); ?>/library/images/Calvary_Church-watch-our-series.jpg">
				<source src="<?php echo get_template_directory_uri(); ?>/library/videos/feature-video.webm" type="video/webm" />
				<source src="<?php echo get_template_directory_uri(); ?>/library/videos/feature-video.mp4" type="video/mp4" />
				<source src="<?php echo get_template_directory_uri(); ?>/library/videos/feature-video.ogv" type="video/ogg" />
			</video>
			<div class="feature-video-content">
				<h2><?php the_field('feature_video_title', false, false); ?></h2>
				<p>
					<a href="<?php the_field('watch_series_cta') ?>" class="btn btn--ghost"><?php the_field('watch_series_text'); ?></a>
					<?php if (in_array('en-US', $classes)): ?>
						<a href="http://live.calvaryonline.org" target="_blank" class="btn btn--ghost">Watch Live</a>
					<?php else: ?>
						<a href="http://live.iglesiacalvario.net/" target="_blank" class="btn btn--ghost">Ver En Vivo</a>
					<?php endif; ?>
				</p>
			</div>
		</section>
		<section class="blade events blade--red">
			<h2><?php the_field('events_title'); ?></h2>
			<div>
				<?php
					$args = array(
						'post_type' => 'events'
					);
					$products = new WP_Query( $args );
					if( $products->have_posts() ) {
						while( $products->have_posts() ) {
							$products->the_post();
				?>

				<div class="slide">
					<div class="slide-content">
						<h3><?php the_field('event_title'); ?></h3>
						<p><?php the_field('event_date'); ?><br><?php the_field('event_hour'); ?></p>
						<a href="<?php the_field('event_cta'); ?>" class="btn btn--ghost"><?php the_field('events_more_info_text'); ?></a>
					</div>
				</div>
				<?php
					  }
					}
					else { echo "Oh No, they aren't events at this moment!"; }
					wp_reset_query();
				?>
			</div>
		</section>
		<section class="blade signup">
			<div class="blade-content">
				<h2 class="signup__title"><?php the_field('subscribe_title'); ?></h2>
				<p class="signup__text"><?php the_field('subscribe_description'); ?></p>
				<form class="signup__form" method="post">
					<?php if ( is_active_sidebar( 'subscribe-bottom' ) ) : ?>
						<?php dynamic_sidebar( 'subscribe-bottom' ); ?>

					<?php else : ?>

						<div class="no-widgets">
							<p><?php _e( 'This is a widget ready area. Add some and they will appear here.', 'bonestheme' );  ?></p>
						</div>

					<?php endif; ?>
				</form>
			</div>
		</section>
	</main>
<?php get_footer(); ?>
