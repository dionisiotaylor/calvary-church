<?php 
/*
	Template Name: Iglesia Calvario Calvary Church
*/
?>
<?php get_header(); ?>
	<main role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
		<section class="hero">
			<?php 
				if ( has_post_thumbnail() ) {
					the_post_thumbnail('full');
				} 
			?>
			<div class="hero__wrapper">
				<div class="hero__cont">
					<h2><?php the_field('hero_title'); ?></h2>
					<p><em><?php the_field('hero_description'); ?></em></p>
				</div>
			</div>
			</div>
		</section>
		<section class="iglesia-calvario">
			<div class="blade white featured">
				<h2><?php the_field('banner_title'); ?></h2>
				<p class="text-center"><?php the_field('banner_description'); ?></p>
			</div>
			<div class="blade">
				<img class="full-width-img" src="<?php the_field('feature_image');?>" alt="" />
				<a class="btn btn--ghost image-cta" href="<?php the_field('cta_feature_link'); ?>"><?php the_field('cta_feature_text'); ?></a>
			</div>
			<div class="blade blade--grid">
				<ul>
					<li>
						<img class="full-width-img" src="<?php echo get_template_directory_uri(); ?>/library/images/iglesia-calvario-retreats.jpg" alt="Calvary Church | Connecting People with God" />
						<div class="blade__cont">
							<h2>retreats</h2>
						</div>
					</li>
					<li>
						<img class="full-width-img" src="<?php echo get_template_directory_uri(); ?>/library/images/iglesia-calvario-bible-studies.jpg" alt="Calvary Church | Connecting People with God" />
						<div class="blade__cont">
							<h2>bible studies</h2>
						</div>
					</li>
					<li>
						<img class="full-width-img" src="<?php echo get_template_directory_uri(); ?>/library/images/iglesia-calvario-family-nights.jpg" alt="Calvary Church | Connecting People with God" />
						<div class="blade__cont">
							<h2>women's group</h2>
						</div>
					</li>
					<li>
						<img class="full-width-img" src="<?php echo get_template_directory_uri(); ?>/library/images/iglesia-calvario-worship.jpg" alt="Calvary Church | Connecting People with God" />
						<div class="blade__cont">
							<h2>worship</h2>
						</div>
					</li>
				</ul>
			</div>
		</section>
	
			<div class="blade blade__small">
				<p class="text-center"><?php the_field('services_description'); ?></p>
				<dl class="services-time">
					<dt><?php the_field('service_field_1'); ?></dt>
					<dd><?php the_field('service_field_2'); ?></dd>

					<dt><?php the_field('service_field_3'); ?></dt>
					<dd><?php the_field('service_field_4'); ?></dd>

					<dt><?php the_field('service_field_5'); ?></dt>
					<dd><?php the_field('service_field_6'); ?></dd>
				</dl>
				<p class="text-center"><a class="btn btn--black" href="<?php the_field('about_cta'); ?>"><?php the_field('about_cta_text'); ?></a></p>
			</div>
	</main>
<?php get_footer(); ?>
