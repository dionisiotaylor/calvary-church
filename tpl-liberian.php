<?php 
/*
	Template Name: Liberian Fellowship Calvary Church
*/
?>
<?php get_header(); ?>
	<main role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
		<section class="hero">
			<?php 
				if ( has_post_thumbnail() ) {
					the_post_thumbnail('full');
				} 
			?>
			<div class="hero__wrapper">
				<div class="hero__cont">
					<h2><?php the_field('hero_title'); ?></h2>
					<p><em><?php the_field('hero_description'); ?></em></p>
				</div>
			</div>
		</section>
		<section>
			<div class="blade blade--columns text-center">
				<div class="container">
					<article>
						<?php the_field('feature_text_left'); ?>
					</article>
					<span class="separator"></span>
					<article>
						<?php the_field('feature_text_right'); ?>
					</article>
				</div>
				<?php the_field('feature_text_center'); ?>
			</div>
			<?php if(get_field('image-bottom')): ?>
				<img class="full-width-img" src="<?php the_field('image-bottom'); ?>" alt="Calvary Church | Connecting People with God" />
			<?php endif;?>
		</section>
	</main>
<?php get_footer(); ?>
