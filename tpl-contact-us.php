<?php 
/*
	Template Name: Contact Us Calvary Church
*/
?>
<?php get_header(); ?>
	<main role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
		<section class="hero">
			<?php 
				if ( has_post_thumbnail() ) {
					the_post_thumbnail('full');
				} 
			?>
			<div class="hero__wrapper">
				<div class="hero__cont">
					<h2><?php the_field('hero_title'); ?></h2>
					<p><em><?php the_field('hero_description'); ?></em></p>
				</div>
			</div>
		</section>
		<section class="kidzone">
			
			<div class="blade blade--contact">
				<?php if ( is_active_sidebar( 'contact-page' ) ) : ?>
					<?php dynamic_sidebar( 'contact-page' ); ?>

				<?php else : ?>

					<div class="no-widgets">
						<p><?php _e( 'This is a widget ready area. Add some and they will appear here.', 'bonestheme' );  ?></p>
					</div>

				<?php endif; ?>
			</div>
			<div class="blade">
				<div id="map-canvas"></div>
			</div>
		</section>
		<div class="blade blade--office">
			<div class="container">
				<div class="col-xs-12 col-sm-4 col-md-3 col-md-offset-2">
					<h2><?php the_field('office_title'); ?></h2>
				</div>
				<div class="col-xs-12 col-sm-8 col-md-5">
					<?php the_field('office_schedule'); ?>
				</div>
			</div>
		</div>
	</main>
<?php get_footer(); ?>
