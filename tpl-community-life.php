<?php 
/*
	Template Name: Community Life Calvary Church
*/
?>
<?php get_header(); ?>
	<main role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
		<section class="hero hero--inner">
			<?php 
				if ( has_post_thumbnail() ) {
					the_post_thumbnail('full');
				} 
			?>
			<div class="hero__wrapper">
				<div class="hero__cont">
					<h2><?php the_field('hero_title'); ?></h2>
					<p><em><?php the_field('hero_description'); ?></em></p>
				</div>
			</div>
		</section>
		<section class="kidzone">
			<div class="blade featured">
				<div class="blade__cont">
					<h2><?php the_field('section_1_title'); ?></h2>
					<p><?php the_field('section_1_description'); ?></p>
				</div>
			</div>
			<div class="wrap-img">
				<?php if( get_field('section_2_image') ): ?>
					<img class="full-width-img" src="<?php the_field('section_2_image'); ?>" alt="Calvary Church | Connecting People with God" />
				<?php endif; ?>
				<div class="blade">
					<div class="blade__cont">
						<h2><?php the_field('section_2_title'); ?></h2>
						<h3><?php the_field('section_2_subtitle'); ?></h3>
						<p><?php the_field('section_2_description'); ?></p>
					</div>
				</div>
			</div>
			<?php if(get_field('section_3_title')): ?>
			<div class="blade gray">
				<div class="blade__cont">
					<h2><?php the_field('section_3_title'); ?></h2>
					<p><?php the_field('section_3_description'); ?></p>
					<a href="<?php the_field('find_group_cta'); ?>" class="btn btn--ghost"><?php the_field('find_group_cta_text'); ?></a>
				</div>
			</div>
			<?php endif; ?>
			<div class="wrap-img">
				<?php if( get_field('section_4_image') ): ?>
					<img class="full-width-img" src="<?php the_field('section_4_image'); ?>" alt="Calvary Church | Connecting People with God" />
				<?php endif; ?>
				<div class="blade">
					<div class="blade__cont">
						<h2><?php the_field('section_4_title'); ?></h2>
					<p><?php the_field('section_4_description'); ?></p>
					</div>
				</div>
			</div>
			<?php if(get_field('section_5_title_left')): ?>
			<div class="blade blade--columns">
				<span class="separator"></span>
				<div class="blade__col">
					<h2><?php the_field('section_5_title_left'); ?></h2>
					<?php the_field('section_5_description_left'); ?>
				</div>
				<div class="blade__col">
					<h2><?php the_field('section_5_title_right'); ?></h2>
					<?php the_field('section_5_description_right'); ?>
				</div>
			</div>
			<?php endif; ?>
			<div class="blade gray signup--connect">
				<div class="blade__cont">
					<h2 class="title-email"><?php the_field('signup_title'); ?></h2>
					<?php the_field('signup_description'); ?>
					<?php if ( is_active_sidebar( 'contact_community' ) ) : ?>
						<?php dynamic_sidebar( 'contact_community' ); ?>

					<?php else : ?>

						<div class="no-widgets">
							<p><?php _e( 'This is a widget ready area. Add some and they will appear here.', 'bonestheme' );  ?></p>
						</div>

					<?php endif; ?>
				</div>
			</div>
			
		</section>
	</main>
<?php get_footer(); ?>
