<?php 
/*
	Template Name: Global Outreach Calvary Church
*/
?>
<?php get_header(); ?>
	<main role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
		<section class="hero">
			<?php 
				if ( has_post_thumbnail() ) {
					the_post_thumbnail('full');
				} 
			?>
			<div class="hero__wrapper">
				<div class="hero__cont">
					<h2><?php the_field('hero_title'); ?></h2>
					<p><em><?php the_field('hero_description'); ?></em></p>
				</div>
			</div>
		</section>
		<section class="kidzone">
			<div class="blade featured-red">
				<div class="blade__cont">
					<p class="text-center"><?php the_field('banner_description'); ?></p>
				</div>
			</div>
			<div class="blade blade--columns">
				<div class="blade__col">
					<h2><?php the_field('1_country_title'); ?></h2>
					<i class="con guatemala"></i>
					<p><?php the_field('1_country_description'); ?></p>
				</div>
				<span class="separator"></span>
				<div class="blade__col">
					<h2><?php the_field('2_country_title'); ?></h2>
					<i class="con china"></i>
					<p><?php the_field('2_country_description'); ?></p>
				</div>
			</div>
			<div class="blade blade--grid">
				<ul>
					<li>
						<img class="full-width-img" src="<?php the_field('1_country_image'); ?>" alt="Calvary Church | Connecting People with God">
					</li>
					<li>
						<img class="full-width-img" src="<?php the_field('2_country_image'); ?>" alt="Calvary Church | Connecting People with God">
					</li>
				</ul>
			</div>
			<div class="blade blade--grid-3">
				<div class="container">
					<div class="col-xs-12 col-sm-4 col-md-4">
						<div class="country">
							<h2><?php the_field('3_country_title'); ?></h2>
							<i class="con short-mission"></i>
							<p><?php the_field('3_country_description'); ?></p>
						</div>
					</div>
					<div class="col-xs-12 col-sm-4 col-md-4">
						<div class="country">
							<h2><?php the_field('4_country_title'); ?></h2>
							<i class="con senegal"></i>
							<p><?php the_field('4_country_description'); ?></p>
						</div>
					</div>
					<div class="col-xs-12 col-sm-4 col-md-4">
						<div class="country">
							<h2><?php the_field('5_country_title'); ?></h2>
							<i class="con liberia"></i>
							<p><?php the_field('5_country_description'); ?></p>
						</div>
					</div>
				</div>
			</div>
			<div class="blade blade--grid">
				<ul>
					<li>
						<img class="full-width-img" src="<?php the_field('3_country_image'); ?>" alt="Calvary Church | Connecting People with God">
					</li>
					<li>
						<img class="full-width-img" src="<?php the_field('4_country_image'); ?>" alt="Calvary Church | Connecting People with God">
					</li>
				</ul>
			</div>
		</section>
	</main>
<?php get_footer(); ?>
