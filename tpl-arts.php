<?php 
/*
	Template Name: Arts Calvary Church
*/
?>
<?php get_header(); ?>
	<main role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
		<section class="hero">
			<?php 
				if ( has_post_thumbnail() ) {
					the_post_thumbnail('full');
				} 
			?>
			<div class="hero__wrapper">
				<div class="hero__cont">
					<h2><?php the_field('hero_title'); ?></h2>
					<h3><?php the_field('hero_subtitle'); ?></h3>
					<p><em><?php the_field('hero_description'); ?></em></p>
				</div>
			</div>
		</section>
		<section class="kidzone">
			<div class="blade blade__small feature-arts">
				<div class="blade__cont">
					<p><?php the_field('banner_description'); ?></p>
				</div>
			</div>
			<div class="wrap-img">
				<img class="full-width-img" src="<?php echo get_template_directory_uri(); ?>/library/images/arts-studio.jpg" alt="Calvary Church | Connecting People with God" />
				<div class="blade">	
					<div class="blade__cont">
						<div class="pattern-splash">
							<h2><?php the_field('section_1_title'); ?></h2>
							<p><?php the_field('section_1_description'); ?></p>
						</div>
					</div>
				</div>
			</div>
			<div class="blade">
				<div class="blade__cont">
					<h2><?php the_field('section_2_title'); ?></h2>
					<p><?php the_field('section_2_description'); ?></p>
				</div>
			</div>
			<img class="full-width-img" src="<?php echo get_template_directory_uri(); ?>/library/images/arts-kids.jpg" alt="Calvary Church | Connecting People with God" />
			<div class="blade gray signup--connect">
				<div class="blade__cont">
					<h2 class="title-email"><?php the_field('signup_title'); ?></h2>
					<p><?php the_field('signup_description'); ?></p>
					<?php if ( is_active_sidebar( 'contact_family_night' ) ) : ?>
						<?php dynamic_sidebar( 'contact_family_night' ); ?>

					<?php else : ?>

						<div class="no-widgets">
							<p><?php _e( 'This is a widget ready area. Add some and they will appear here.', 'bonestheme' );  ?></p>
						</div>

					<?php endif; ?>
				</div>
			</div>
		</section>
	</main>
<?php get_footer(); ?>
