<?php 
/*
	Template Name: Give Calvary Church
*/
?>
<?php get_header(); ?>
	<main role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
		<section class="hero">
			<?php 
				if ( has_post_thumbnail() ) {
					the_post_thumbnail('full');
				} 
			?>
			<div class="hero__wrapper">
				<div class="hero__cont">
					<h2><?php the_field('hero_title'); ?></h2>
					<p><em><?php the_field('hero_description'); ?></em></p>
				</div>
			</div>
		</section>
		<section class="kidzone">
			<div class="blade">
				
				<div class="blade__cont">
					<h2><?php the_field('feature_section_title'); ?></h2>
					<p><?php the_field('feature_section_description'); ?></p>
				</div>
			</div>
			
			<div class="blade red blade__small">
				<div class="blade__cont">
					<h2 class="secondary-title text-small"><?php the_field('banner_title'); ?></h2>
					<p><?php the_field('banner_description'); ?></p>
				</div>
			</div>
			<div class="blade blade__small">
				<div class="blade__cont">
					<h2><?php the_field('section_steps_title'); ?></h2>
					<ul class="give-steps">
						<li>
							<h3>1</h3>
							<p><?php the_field('section_steps_1'); ?></p>
						</li>
						<li>
							<h3>2</h3>
							<p><?php the_field('section_steps_2'); ?></p>
						</li>
						<li>
							<h3>3</h3>
							<p><?php the_field('section_steps_3'); ?></p>
						</li>
					</ul>
					<p><?php the_field('section_steps_disclaimer'); ?></p>
				</div>
			</div>
			<div class="blade red blade__small">
				<a href="<?php the_field('cta_give_online'); ?>" class="btn btn--ghost"><?php the_field('cta_give_online_text'); ?></a>
			</div>
			<div class="blade gray">
				<div class="blade__cont">
					<h2><?php the_field('section_donations_title'); ?></h2>
					<p><?php the_field('section_donations_description'); ?></p>
					<a href="<?php the_field('section_donations_cta'); ?>" class="btn btn--ghost"><?php the_field('section_donations_cta_text'); ?></a>
				</div>
			</div>
		</section>
	</main>
<?php get_footer(); ?>
