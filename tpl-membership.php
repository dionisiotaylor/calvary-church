<?php 
/*
	Template Name: Membership Calvary Church
*/
?>
<?php get_header(); ?>
	<main role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
		<section class="hero">
			<?php 
				if ( has_post_thumbnail() ) {
					the_post_thumbnail('full');
				} 
			?>
			<div class="hero__wrapper">
				<div class="hero__cont">
					<h2><?php the_field('hero_title'); ?></h2>
					<p><em><?php the_field('hero_description'); ?></em></p>
				</div>
			</div>
		</section>
		<section class="kidzone">
			<div class="blade blade--columns">
				<h2><?php the_field('banner_title'); ?></h2>
				<div class="blade__col">
					<article>
						<?php the_field('banner_description_left'); ?>
					</article>
				</div>
				<div class="blade__col">
					<article>
						<?php the_field('banner_description_right'); ?>
					</article>
				</div>
			</div>
			<div class="wrap-img">
				<img class="full-width-img" src="<?php the_field('1_section_image'); ?>" alt="Calvary Church | Connecting People with God">
				<div class="blade blade__columns">
					<span class="list-type-number">1</span>
					<h2><?php the_field('1_section_title'); ?></h2>
					<div class="blade__col">
						<article>
							<?php the_field('1_section_description_left'); ?>
						</article>
					</div>
					<div class="blade__col">
						<article>
							<?php the_field('1_section_description_right'); ?>
						</article>
					</div>
				</div>
			</div>
			<div class="blade red blade__small">
				<a href="<?php the_field('baptism_class_cta'); ?>" class="btn btn--ghost"><?php the_field('baptism_class_cta_text'); ?></a>
			</div>
			<div class="wrap-img">
				<img class="full-width-img" src="<?php the_field('2_section_image'); ?>" alt="Calvary Church | Connecting People with God">
				<div class="blade">
					<div class="blade__cont">
						<span class="list-type-number">2</span>
						<h2><?php the_field('2_section_title'); ?></h2>
						<p><?php the_field('2_section_description'); ?></p>
					</div>
				</div>
			</div>
			<div class="blade red blade__small">
				<a href="<?php the_field('download_booklet_cta'); ?>" class="btn btn--ghost"><?php the_field('download_booklet_cta_text'); ?></a>
			</div>
			<div class="wrap-img">
				<img class="full-width-img" src="<?php the_field('3_section_image'); ?>" alt="Calvary Church | Connecting People with God">
				<div class="blade">
					<div class="blade__cont">
						<span class="list-type-number">3</span>
						<h2><?php the_field('3_section_title'); ?></h2>
						<p><?php the_field('3_section_description'); ?></p>
					</div>
				</div>
			</div>
		</section>
	</main>
<?php get_footer(); ?>
