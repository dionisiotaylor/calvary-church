<?php 
/*
	Template Name: Volunteer Calvary Church
*/
$classes = get_body_class();
?>
<?php get_header(); ?>
	<main role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
		<section class="hero">
			<?php 
				if ( has_post_thumbnail() ) {
					the_post_thumbnail('full');
				} 
			?>
			<div class="hero__wrapper">
				<div class="hero__cont">
					<h2><?php the_field('hero_title'); ?></h2>
					<p><em><?php the_field('hero_description'); ?></em></p>
				</div>
			</div>
		</section>
		<section class="kidzone">
			<div class="blade blade__small">
				<div class="blade__cont">
					<h2><?php the_field('feature_title'); ?></h2>
					<p><?php the_field('feature_description'); ?></p>
				</div>
			</div>
			<div class="blade">
				<ul class="full-width-list">
					<li>
						<div class="volunteer-info">
							<div class="full-width-list__info">
								<h2><?php the_field('1_section_title'); ?></h2>
								<p><?php the_field('1_section_description'); ?></p>
								<?php if (in_array('es-ES', $classes) ): ?>
									<a href="" class="btn btn--ghost">Aplicar Ahora</a>
								<?php else: ?>
									<a href="" class="btn btn--ghost">Apply now</a>
								<?php endif; ?>
							</div>
							<div class="full-width-list__media">
								<img class="full-width-img" src="<?php the_field('1_section_image'); ?>" alt="Calvary Church | Connecting People with God" />
							</div>
						</div>
						<div class="volunteer-form">
							<?php 
								$classes = get_body_class();
								if (in_array('es-ES', $classes)) {
									if ( is_active_sidebar( 'voluntarios-ninos' ) ) { 
										dynamic_sidebar( 'voluntarios-ninos' );
									}
								} elseif(in_array('en-US', $classes)) {
									if ( is_active_sidebar( 'volunteer-kids' ) ) { 
										dynamic_sidebar( 'volunteer-kids' );
									}
								} 
							?>
							
						</div>
					</li>
					<li>
						<div class="volunteer-info">
							<div class="full-width-list__info">
								<h2><?php the_field('2_section_title'); ?></h2>
								<p><?php the_field('2_section_description'); ?></p>
								<?php if (in_array('es-ES', $classes) ): ?>
									<a href="" class="btn btn--ghost">Aplicar Ahora</a>
								<?php else: ?>
									<a href="" class="btn btn--ghost">Apply now</a>
								<?php endif; ?>
							</div>
							<div class="full-width-list__media">
								<img class="full-width-img" src="<?php the_field('2_section_image'); ?>" alt="Calvary Church | Connecting People with God" />
							</div>
						</div>
						<div class="volunteer-form">
							<?php 
								$classes = get_body_class();
								if (in_array('es-ES', $classes)) {
									if ( is_active_sidebar( 'tecnologia' ) ) { 
										dynamic_sidebar( 'tecnologia' );
									}
								} elseif(in_array('en-US', $classes)) {
									if ( is_active_sidebar( 'first-impressions' ) ) { 
										dynamic_sidebar( 'first-impressions' );
									}
								} 
							?>
						</div>
					</li>
					<li>
						<div class="volunteer-info">
							<div class="full-width-list__info">
								<h2><?php the_field('3_section_title'); ?></h2>
								<p><?php the_field('3_section_description'); ?></p>
								<?php if (in_array('es-ES', $classes) ): ?>
									<a href="" class="btn btn--ghost">Aplicar Ahora</a>
								<?php else: ?>
									<a href="" class="btn btn--ghost">Apply now</a>
								<?php endif; ?>
							</div>
							<div class="full-width-list__media">
								<img class="full-width-img" src="<?php the_field('3_section_image'); ?>" alt="Calvary Church | Connecting People with God" />
							</div>
						</div>
						<div class="volunteer-form">
							<?php 
								
								if (in_array('es-ES', $classes)) {
									if ( is_active_sidebar( 'equipo-canvus' ) ) { 
										dynamic_sidebar( 'equipo-canvus' );
									}
								} elseif(in_array('en-US', $classes)) {
									if ( is_active_sidebar( 'technical-arts' ) ) { 
										dynamic_sidebar( 'technical-arts' );
									}
								} 
							?>
						</div>
					</li>
					<li>
						<div class="volunteer-info">
							<div class="full-width-list__info">
								<h2><?php the_field('4_section_title'); ?></h2>
								<p><?php the_field('4_section_description'); ?></p>
								<?php if (in_array('es-ES', $classes) ): ?>
									<a href="" class="btn btn--ghost">Aplicar Ahora</a>
								<?php else: ?>
									<a href="" class="btn btn--ghost">Apply now</a>
								<?php endif; ?>
							</div>
							<div class="full-width-list__media">
								<img class="full-width-img" src="<?php the_field('4_section_image'); ?>" alt="Calvary Church | Connecting People with God" />
							</div>
						</div>
						<div class="volunteer-form">
							<?php 
								
								if (in_array('es-ES', $classes)) {
									if ( is_active_sidebar( 'adoracion' ) ) { 
										dynamic_sidebar( 'adoracion' );
									}
								} elseif(in_array('en-US', $classes)) {
									if ( is_active_sidebar( 'canv-us-team' ) ) { 
										dynamic_sidebar( 'canv-us-team' );
									}
								} 
							?>
						</div>
					</li>
				</ul>
			</div>
		</section>
	</main>
<?php get_footer(); ?>
