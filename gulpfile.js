'use strict';
 
var gulp			= require('gulp');
var sass 			= require('gulp-sass');
var rename 			= require("gulp-rename");
var clean 			= require('gulp-clean');
var cmq				= require('gulp-combine-media-queries');
var autoprefixer 	= require('gulp-autoprefixer');

gulp.task('styles', function() {
    gulp.src('library/scss/main.sass')
        .pipe(sass().on('error', sass.logError))
		.pipe(rename('style.css'))
		.pipe(autoprefixer({
            browsers: ['last 2 versions', 'ie 9'],
            cascade: false
        }))
		.pipe(cmq({log: true}))
        .pipe(gulp.dest('library/css/'))
		
});


gulp.task('clean-css-folder', function () {
  return gulp.src('library/css/style.css', {read: false})
    .pipe(clean());
});
 
gulp.task('clean-styles', ['clean-css-folder'], function () {
  gulp.src('library/css/style.css')
    .pipe(gulp.dest('library/css'));
});
 

// Default task
gulp.task('default', ['clean-styles'], function() {
    gulp.watch('library/scss/**/*.sass',['styles']);
});