<?php 
/*
	Template Name: Student Ministries Calvary Church
*/
?>
<?php get_header(); ?>
	<main role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
		<section class="hero">
			<?php 
				if ( has_post_thumbnail() ) {
					the_post_thumbnail('full');
				} 
			?>
			<div class="hero__wrapper">
				<div class="hero__cont">
					<h2><?php the_field('hero_title'); ?></h2>
					<p><em><?php the_field('hero_description'); ?></em></p>
				</div>
			</div>
		</section>
		<section class="kidzone">
			<div class="blade red blade__small">
				<p><?php the_field('banner_description'); ?></p>
			</div>
			<section class="blade side-video">
				<div class="side-video__text text-center">
					<h2><?php the_field('section_1_title'); ?></h2>
					<h3><?php the_field('section_1_subtitle'); ?></h3>
					<p><?php the_field('section_1_description'); ?></p>
				</div>
			</section>
			<div class="wrap-img">
				<img class="full-width-img" src="<?php echo get_template_directory_uri(); ?>/library/images/student-ministries-breakaway.jpg" alt="Calvary Church | Connecting People with God" />
				<div class="blade">
					<div class="blade__cont">
						<h2><?php the_field('section_2_title'); ?></h2>
						<h3><?php the_field('section_2_subtitle'); ?></h3>
						<p><?php the_field('section_2_description'); ?></p>
					</div>
				</div>
			</div>
		</section>
		
		<section class="blade featured-headline">
			<h2><?php the_field('section_3_title'); ?></h2>
			<h3><?php the_field('section_3_subtitle'); ?></h3>
			<p><?php the_field('section_3_description'); ?></p>
		</section>
		<img class="full-width-img" src="<?php echo get_template_directory_uri(); ?>/library/images/student-ministries-high-school.jpg" alt="Calvary Church | Connecting People with God" />
	</main>
<?php get_footer(); ?>
