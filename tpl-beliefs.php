<?php 
/*
	Template Name: Beliefs Calvary Church
*/
?>
<?php get_header(); ?>
	<main role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
		<section class="hero">
			<?php 
				if ( has_post_thumbnail() ) {
					the_post_thumbnail('full');
				} 
			?>
			<div class="hero__wrapper">
				<div class="hero__cont">
					<h2><?php the_field('hero_title'); ?></h2>
					<p><em><?php the_field('hero_description'); ?></em></p>
					<i class="con cross"></i>
				</div>
			</div>
		</section>
		<section class="beliefs">
			<div class="blade">
				<div class="container">
					<div class="col-xs-12 col-sm-6 col-md-6">
						<h2><?php the_field('blade_1_title_left'); ?></h2>
						<p><?php the_field('blade_1_description_left'); ?></p>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-6">
						<h2><?php the_field('blade_1_title_right'); ?></h2>
						<p><?php the_field('blade_1_description_right'); ?></p>
					</div>
				</div>
			</div>
			<div class="blade red">
				<div class="container">
					<div class="col-xs-12 col-sm-6 col-md-6">
						<h2><?php the_field('blade_2_title_left'); ?></h2>
						<p><?php the_field('blade_2_description_left'); ?></p>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-6">
						<h2><?php the_field('blade_2_title_right'); ?></h2>
						<p><?php the_field('blade_2_description_right'); ?></p>
					</div>
				</div>
			</div>
			
			<div class="blade">
				<div class="container">
					<div class="col-xs-12 col-sm-6 col-md-6">
						<h2><?php the_field('blade_3_title_left'); ?></h2>
						<p><?php the_field('blade_3_description_left'); ?></p>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-6">
						<h2><?php the_field('blade_3_title_right'); ?></h2>
						<p><?php the_field('blade_3_description_right'); ?></p>
					</div>
				</div>
			</div>
			<div class="blade red">
				<div class="container">
					<div class="col-xs-12 col-sm-6 col-md-6">
						<h2><?php the_field('blade_4_title_left'); ?></h2>
						<p><?php the_field('blade_4_description_left'); ?></p>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-6">
						<h2><?php the_field('blade_4_title_right'); ?></h2>
						<p><?php the_field('blade_4_description_right'); ?></p>
					</div>
				</div>
			</div>
			
			<div class="blade">
				<div class="container">
					<div class="col-xs-12 col-sm-6 col-md-6">
						<h2><?php the_field('blade_5_title_left'); ?></h2>
						<p><?php the_field('blade_5_description_left'); ?></p>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-6">
						<h2><?php the_field('blade_5_title_right'); ?></h2>
						<p><?php the_field('blade_5_description_right'); ?></p>
					</div>
				</div>
			</div>
			
		</section>
	</main>
<?php get_footer(); ?>
