<?php 
/*
	Template Name: Local Outreach Calvary Church
*/
?>
<?php get_header(); ?>
	<main role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
		<section class="hero">
			<?php 
				if ( has_post_thumbnail() ) {
					the_post_thumbnail('full');
				} 
			?>
			<div class="hero__wrapper">
				<div class="hero__cont">
					<h2><?php the_field('hero_title'); ?></h2>
					<p><em><?php the_field('hero_description'); ?></em></p>
				</div>
			</div>
		</section>
		<section class="kidzone">
			<div class="blade featured-gray">
				<div class="blade__cont">
					<p><?php the_field('banner_description'); ?></p>
				</div>
			</div>
			<div class="blade red side-images">
				<div class="container">
					<div class="col-xs-12 col-sm-3 side-images__media">
						<img class="full-width-img left" src="<?php the_field('section_1_image_left'); ?>" alt="Calvary Church | Connecting People with God" />
					</div>
					<div class="col-xs-12 col-sm-6 side-images__text">
						<h2><?php the_field('section_1_title'); ?></h2>
						<p><?php the_field('section_1_description'); ?></p>
					</div>
					<div class="col-xs-12 col-sm-3 side-images__media">
						<img class="full-width-img right" src="<?php the_field('section_1_image_right'); ?>" alt="Calvary Church | Connecting People with God" />
					</div>
				</div>
			</div>
			<div class="blade side-images">
				<div class="container">
					<div class="col-md-3 col-sm-3 side-images__media">
						<img class="full-width-img left" src="<?php the_field('section_2_image_left'); ?>" alt="Calvary Church | Connecting People with God" />
					</div>
					<div class="col-md-6 col-sm-6 side-images__text">
						<h2><?php the_field('section_2_title'); ?></h2>
						<p><?php the_field('section_2_description'); ?></p>
					</div>
					<div class="col-md-3 col-sm-3 side-images__media">
						<img class="full-width-img right" src="<?php the_field('section_2_image_right'); ?>" alt="Calvary Church | Connecting People with God" />
					</div>
				</div>
			</div>
			<div class="blade gray side-images">
				<div class="container">
					<div class="col-md-3 col-sm-3 side-images__media">
						<img class="full-width-img left" src="<?php the_field('section_3_image_left'); ?>" alt="Calvary Church | Connecting People with God" />
					</div>
					<div class="col-md-6 col-sm-6 side-images__text">
						<h2><?php the_field('section_3_title'); ?></h2>
						<p><?php the_field('section_3_description'); ?></p>
					</div>
					<div class="col-md-3 col-sm-3 side-images__media">
						<img class="full-width-img right" src="<?php the_field('section_3_image_right'); ?>" alt="Calvary Church | Connecting People with God" />
					</div>
				</div>
			</div>
			<?php if(get_field('section_4_title')) :?>
				<div class="blade side-images">
					<div class="container">
						<div class="col-xs-12 col-sm-3 side-images__media">
							<img class="full-width-img left" src="<?php the_field('section_4_image_left'); ?>" alt="Calvary Church | Connecting People with God" />
						</div>
						<div class="col-xs-12 col-sm-6 side-images__text">
							<h2><?php the_field('section_4_title'); ?></h2>
							<p><?php the_field('section_4_description'); ?></p>
						</div>
						<div class="col-xs-12 col-sm-3 side-images__media">
							<img class="full-width-img right" src="<?php the_field('section_4_image_right'); ?>" alt="Calvary Church | Connecting People with God" />
						</div>
					</div>
				</div>
			<?php endif; ?>
			<?php if(get_field('section_5_title')) :?>
				<div class="blade gray side-images">
					<div class="container">
						<div class="col-xs-12 col-sm-3 side-images__media">
							<img class="full-width-img left" src="<?php the_field('section_5_image_left'); ?>" alt="Calvary Church | Connecting People with God" />
						</div>
						<div class="col-xs-12 col-sm-6 side-images__text">
							<h2><?php the_field('section_5_title'); ?></h2>
							<p><?php the_field('section_5_description'); ?></p>
						</div>
						<div class="col-xs-12 col-sm-3 side-images__media">
							<img class="full-width-img right" src="<?php the_field('section_5_image_right'); ?>" alt="Calvary Church | Connecting People with God" />
						</div>
					</div>
				</div>
			<?php endif; ?>
		</section>
	</main>
<?php get_footer(); ?>
