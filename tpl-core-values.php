<?php 
/*
	Template Name: Core Values Calvary Church
*/
?>
<?php get_header(); ?>
	<main role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
		<section class="hero">
			<?php 
				if ( has_post_thumbnail() ) {
					the_post_thumbnail('full');
				} 
			?>
			<div class="hero__wrapper">
				<div class="hero__cont">
					<h2><?php the_field('hero_title'); ?></h2>
					<p><em><?php the_field('hero_description'); ?></em></p>
				</div>
			</div>
		</section>
		<section class="core-values">
			<div class="blade">
				<div class="container">
					<div class="col-xs-12 col-md-3 col-sm-4 col-md-offset-3">
						<div class="center">
							<div class="center__cont">
								<i class="con belong"></i>
								<h2 class="value-icon">1</h2>
							</div>
						</div>
					</div>
					<div class="col-xs-12 col-sm-8 col-md-4">
						<div class="center">
							<div class="center__cont">
								<h3><?php the_field('value_#1_title'); ?></h3>
								<p><?php the_field('value_#1_description'); ?></p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="blade">
				<div class="container">
					<div class="col-xs-12 col-md-5 col-sm-4 right">
						<div class="center">
							<div class="center__cont">
								<h2 class="value-icon">2</h2>
								<i class="con worship"></i>
							</div>
						</div>
					</div>
					<div class="col-xs-12 col-sm-8 col-md-4 col-md-offset-2">
						<div class="center">
							<div class="center__cont">
								<h3><?php the_field('value_#2_title'); ?></h3>
								<p><?php the_field('value_#2_description'); ?></p>
							</div>
						</div>
					</div>
				</div>
			</div>
			
			<div class="blade">
				<div class="container">
					<div class="col-xs-12 col-md-3 col-sm-4 col-md-offset-3">
						<div class="center">
							<div class="center__cont">
								<i class="con community"></i>
								<h2 class="value-icon">3</h2>
							</div>
						</div>
					</div>
					<div class="col-xs-12 col-sm-8 col-md-4">
						<div class="center">
							<div class="center__cont">
								<h3><?php the_field('value_#3_title'); ?></h3>
								<p><?php the_field('value_#3_description'); ?></p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="blade">
				<div class="container">
					<div class="col-xs-12 col-md-5 col-sm-4 right">
						<div class="center">
							<div class="center__cont">
								<h2 class="value-icon">4</h2>
								<i class="con instruction"></i>
							</div>
						</div>
					</div>
					<div class="col-xs-12 col-sm-8 col-md-4 col-md-offset-2">
						<div class="center">
							<div class="center__cont">
								<h3><?php the_field('value_#4_title'); ?></h3>
								<p><?php the_field('value_#4_description'); ?></p>
							</div>
						</div>
					</div>
				</div>
			</div>
			
			<div class="blade">
				<div class="container">
					<div class="col-xs-12 col-md-3 col-sm-4 col-md-offset-3">
						<div class="center">
							<div class="center__cont">
								<i class="con prayer"></i>
								<h2 class="value-icon">5</h2>
							</div>
						</div>
					</div>
					<div class="col-xs-12 col-sm-8 col-md-4">
						<div class="center">
							<div class="center__cont">
								<h3><?php the_field('value_#5_title'); ?></h3>
								<p><?php the_field('value_#5_description'); ?></p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="blade">
				<div class="container">
					<div class="col-xs-12 col-md-5 col-sm-4 right">
						<div class="center">
							<div class="center__cont">
								<h2 class="value-icon">6</h2>
								<i class="con service"></i>
							</div>
						</div>
					</div>
					<div class="col-xs-12 col-sm-8 col-md-4 col-md-offset-2">
						<div class="center">
							<div class="center__cont">
								<h3><?php the_field('value_#6_title'); ?></h3>
								<p><?php the_field('value_#6_description'); ?></p>
							</div>
						</div>
					</div>
				</div>
			</div>
			
			<div class="blade">
				<div class="container">
					<div class="col-xs-12 col-md-3 col-sm-4 col-md-offset-3">
						<div class="center">
							<div class="center__cont">
								<i class="con stewardship"></i>
								<h2 class="value-icon">7</h2>
							</div>
						</div>
					</div>
					<div class="col-xs-12 col-sm-8 col-md-4">
						<div class="center">
							<div class="center__cont">
								<h3><?php the_field('value_#7_title'); ?></h3>
								<p><?php the_field('value_#7_description'); ?></p>
							</div>
						</div>
					</div>
				</div>
			</div>
			
		</section>
	</main>
<?php get_footer(); ?>
