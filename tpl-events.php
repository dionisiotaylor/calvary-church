<?php 
/*
	Template Name: Events Calvary Church
*/
?>
<?php get_header(); ?>
	<main role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
		<section class="hero">
			<?php 
				if ( has_post_thumbnail() ) {
					the_post_thumbnail('full');
				} 
			?>
			<div class="hero__wrapper">
				<div class="hero__cont">
					<h2><?php the_field('hero_title'); ?></h2>
					<p><em><?php the_field('hero_description'); ?></em></p>
				</div>
			</div>
		</section>
		<div class="events">
			<ul class="events__row">
				<?php
					$args = array(
						'post_type'		=> 'events',
						'orderby'		=> 'menu_order'
					);
					$products = new WP_Query( $args );
					if( $products->have_posts() ) {
						while( $products->have_posts() ) {
							$products->the_post();
				?>
							<li>
								<?php
									$event_date 	= get_field('event_date');
									$event_hour 	= get_field('event_hour');
									$event_location = get_field('event_location');
									$event_info 	= get_field('events_info');
									$event_image 	= get_field('events_more_info_image');
								?>
								<?php if($event_image): ?>
									<figure class="events__img"><a href="<?php the_field('events_more_info_image'); ?>" class="mfp-image"><img class="full-width-img" src="<?php the_field('event_image'); ?>" alt="Calvary Church | <?php the_field('event_title'); ?>" /></a></figure>
								<?php else: ?>
									<figure class="events__img"><img class="full-width-img" src="<?php the_field('event_image'); ?>" alt="Calvary Church | <?php the_field('event_title'); ?>" /></figure>
								<?php endif; ?>
								<div class="events__text">
									<h3><?php the_field('event_title'); ?></h3>
									<p><?php the_field('event_description'); ?></p>
									<ul class="info">
									
									
										<?php if ( $event_date ) : ?>
											<li><i class="con calendar"></i><?php the_field('event_date'); ?></li>
										<?php endif; ?>

										<?php if ( $event_hour ) : ?>
											<li><i class="con pin"></i><?php the_field('event_hour'); ?></li>
										<?php endif; ?>
										
										<?php if ( $event_location ) : ?>
											<li><i class="con clock"></i><?php the_field('event_location'); ?></li>
										<?php endif; ?>
										
										<?php if ( $event_info ) : ?>
											<li><i class="con mail"></i><?php the_field('events_info'); ?></li>
										<?php endif; ?>
										
										<?php if ( $event_image ) : ?>
											<li><i class="con pin2"></i><p><a href="<?php the_field('events_more_info_image'); ?>" class="mfp-image"><?php the_field('events_more_info_text'); ?></a></p></li>
										<?php endif; ?>
										
									</ul>
								</div>
							</li>
						<?php
					  }
					}
					else { echo 'Oh ohm no products!'; }
					wp_reset_query();
				?>
			</ul>
		</div>
	</main>
<?php get_footer(); ?>
