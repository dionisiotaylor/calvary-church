<?php
/*
Author: Eddie Machado
URL: http://themble.com/bones/

This is where you can drop your custom functions or
just edit things like thumbnail sizes, header images,
sidebars, comments, ect.
*/

// LOAD BONES CORE (if you remove this, the theme will break)
require_once( 'library/bones.php' );

// CUSTOMIZE THE WORDPRESS ADMIN (off by default)
// require_once( 'library/admin.php' );

/*********************
LAUNCH BONES
Let's get everything up and running.
*********************/

function bones_ahoy() {

  //Allow editor style.
  add_editor_style( get_stylesheet_directory_uri() . '/library/css/editor-style.css' );

  // let's get language support going, if you need it
  load_theme_textdomain( 'bonestheme', get_template_directory() . '/library/translation' );

  // USE THIS TEMPLATE TO CREATE CUSTOM POST TYPES EASILY
  require_once( 'library/custom-post-type.php' );

  // launching operation cleanup
  add_action( 'init', 'bones_head_cleanup' );
  // A better title
  add_filter( 'wp_title', 'rw_title', 10, 3 );
  // remove WP version from RSS
  add_filter( 'the_generator', 'bones_rss_version' );
  // remove pesky injected css for recent comments widget
  add_filter( 'wp_head', 'bones_remove_wp_widget_recent_comments_style', 1 );
  // clean up comment styles in the head
  add_action( 'wp_head', 'bones_remove_recent_comments_style', 1 );
  // clean up gallery output in wp
  add_filter( 'gallery_style', 'bones_gallery_style' );

  // enqueue base scripts and styles
  add_action( 'wp_enqueue_scripts', 'bones_scripts_and_styles', 999 );
  // ie conditional wrapper

  // launching this stuff after theme setup
  bones_theme_support();

  // adding sidebars to Wordpress (these are created in functions.php)
  add_action( 'widgets_init', 'bones_register_sidebars' );
  
	
  add_action( 'init', 'register_my_menus' );
  
  // Launching CPT
  add_action( 'init', 'event_post_type', 0 );
  add_action( 'init', 'staff_post_type', 0 );
  add_action( 'init', 'watchseries_post_type', 0 );
	
  // Adding Languages for CPT
  add_filter('pll_get_post_types', 'my_pll_get_post_types');
	
  // cleaning up random code around images
  add_filter( 'the_content', 'bones_filter_ptags_on_images' );
  // cleaning up excerpt
  add_filter( 'excerpt_more', 'bones_excerpt_more' );
  
  //remove_filter ('acf_the_content', 'wpautop');
  
  // Add class to the body for the languages
  add_filter('body_class', 'my_custom_body_class', 10, 2);
  function my_custom_body_class($classes) {
    $classes[] = get_bloginfo('language');
    return $classes;
  }
} /* end bones ahoy */


// let's get this party started
add_action( 'after_setup_theme', 'bones_ahoy' );


/************* OEMBED SIZE OPTIONS *************/

if ( ! isset( $content_width ) ) {
	$content_width = 640;
}

/************* THUMBNAIL SIZE OPTIONS *************/

// Thumbnail sizes
add_image_size( 'bones-thumb-600', 600, 150, true );
add_image_size( 'bones-thumb-300', 300, 100, true );

/*
to add more sizes, simply copy a line from above
and change the dimensions & name. As long as you
upload a "featured image" as large as the biggest
set width or height, all the other sizes will be
auto-cropped.

To call a different size, simply change the text
inside the thumbnail function.

For example, to call the 300 x 100 sized image,
we would use the function:
<?php the_post_thumbnail( 'bones-thumb-300' ); ?>
for the 600 x 150 image:
<?php the_post_thumbnail( 'bones-thumb-600' ); ?>

You can change the names and dimensions to whatever
you like. Enjoy!
*/

add_filter( 'image_size_names_choose', 'bones_custom_image_sizes' );

function bones_custom_image_sizes( $sizes ) {
    return array_merge( $sizes, array(
        'bones-thumb-600' => __('600px by 150px'),
        'bones-thumb-300' => __('300px by 100px'),
    ) );
}

/*
The function above adds the ability to use the dropdown menu to select
the new images sizes you have just created from within the media manager
when you add media to your content blocks. If you add more image sizes,
duplicate one of the lines in the array and name it according to your
new image size.
*/

/************* THEME CUSTOMIZE *********************/

/* 
  A good tutorial for creating your own Sections, Controls and Settings:
  http://code.tutsplus.com/series/a-guide-to-the-wordpress-theme-customizer--wp-33722
  
  Good articles on modifying the default options:
  http://natko.com/changing-default-wordpress-theme-customization-api-sections/
  http://code.tutsplus.com/tutorials/digging-into-the-theme-customizer-components--wp-27162
  
  To do:
  - Create a js for the postmessage transport method
  - Create some sanitize functions to sanitize inputs
  - Create some boilerplate Sections, Controls and Settings
*/

function bones_theme_customizer($wp_customize) {
  // $wp_customize calls go here.
  //
  // Uncomment the below lines to remove the default customize sections 

  // $wp_customize->remove_section('title_tagline');
  // $wp_customize->remove_section('colors');
  // $wp_customize->remove_section('background_image');
  // $wp_customize->remove_section('static_front_page');
  // $wp_customize->remove_section('nav');

  // Uncomment the below lines to remove the default controls
  // $wp_customize->remove_control('blogdescription');
  
  // Uncomment the following to change the default section titles
  // $wp_customize->get_section('colors')->title = __( 'Theme Colors' );
  // $wp_customize->get_section('background_image')->title = __( 'Images' );
}

add_action( 'customize_register', 'bones_theme_customizer' );

/************* ACTIVE SIDEBARS ********************/

// Sidebars & Widgetizes Areas
function bones_register_sidebars() {
//	register_sidebar(array(
//		'id' => 'sidebar1',
//		'name' => __( 'Sidebar 1', 'bonestheme' ),
//		'description' => __( 'The first (primary) sidebar.', 'bonestheme' ),
//		'before_widget' => '<div id="%1$s" class="widget %2$s">',
//		'after_widget' => '</div>',
//		'before_title' => '<h4 class="widgettitle">',
//		'after_title' => '</h4>',
//	));

	/*
	to add more sidebars or widgetized areas, just copy
	and edit the above sidebar code. In order to call
	your new sidebar just use the following code:

	Just change the name to whatever your new
	sidebar's id is, for example:

	register_sidebar(array(
		'id' => 'sidebar2',
		'name' => __( 'Sidebar 2', 'bonestheme' ),
		'description' => __( 'The second (secondary) sidebar.', 'bonestheme' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h4 class="widgettitle">',
		'after_title' => '</h4>',
	));

	To call the sidebar in your template, you can just copy
	the sidebar.php file and rename it to your sidebar's name.
	So using the above example, it would be:
	sidebar-sidebar2.php

	*/
	
	
	// Register Subscribe Widget
	register_sidebar(array(
		'id' => 'subscribe-bottom',
		'name' => __( 'Subscribe Bottom', 'bonestheme' ),
		'description' => __( 'Subscribe widget at the bottom', 'bonestheme' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h4 class="widgettitle">',
		'after_title' => '</h4>',
	));
	register_sidebar(array(
		'id' => 'subscribe-unete',
		'name' => __( 'Subscribite Unete', 'bonestheme' ),
		'description' => __( 'Subscribite Unete at the bottom of the homepage', 'bonestheme' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h4 class="widgettitle">',
		'after_title' => '</h4>',
	));
	
	
	
	// Contact Form
	register_sidebar(array(
		'id' => 'contact-page',
		'name' => __( 'Contact Form', 'bonestheme' ),
		'description' => __( 'Contact Form widget at Contact us Page', 'bonestheme' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h4 class="widgettitle">',
		'after_title' => '</h4>'
	));
	
	// Volunteers Kids
	register_sidebar(array(
		'id' => 'volunteer-kids',
		'name' => __( 'Volunteers Kids', 'bonestheme' ),
		'description' => __( 'Volunteers Kids Form', 'bonestheme' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h4 class="widgettitle">',
		'after_title' => '</h4>'
	));
	register_sidebar(array(
		'id' => 'voluntarios-ninos',
		'name' => __( 'Voluntarios Ninos', 'bonestheme' ),
		'description' => __( 'Formulario Voluntarios Ninos', 'bonestheme' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h4 class="widgettitle">',
		'after_title' => '</h4>'
	));
	
	// First Impressions
	register_sidebar(array(
		'id' => 'first-impressions',
		'name' => __( 'First Impressions', 'bonestheme' ),
		'description' => __( 'First Impressions Form', 'bonestheme' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h4 class="widgettitle">',
		'after_title' => '</h4>'
	));
	
	// Technical Arts
	register_sidebar(array(
		'id' => 'technical-arts',
		'name' => __( 'Technical Arts', 'bonestheme' ),
		'description' => __( 'Technical Arts Form', 'bonestheme' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h4 class="widgettitle">',
		'after_title' => '</h4>'
	));
	register_sidebar(array(
		'id' => 'tecnologia',
		'name' => __( 'Tecnologia', 'bonestheme' ),
		'description' => __( 'Tecnologia Formulario', 'bonestheme' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h4 class="widgettitle">',
		'after_title' => '</h4>'
	));
	
	// Canv-us Team
	register_sidebar(array(
		'id' => 'canvus-team',
		'name' => __( 'Can-us Team', 'bonestheme' ),
		'description' => __( 'Can-us Team Form', 'bonestheme' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h4 class="widgettitle">',
		'after_title' => '</h4>'
	));
	register_sidebar(array(
		'id' => 'equipo-canvus',
		'name' => __( 'Equipo Canv-us', 'bonestheme' ),
		'description' => __( 'Equipo Canv-us Form', 'bonestheme' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h4 class="widgettitle">',
		'after_title' => '</h4>'
	));
	
	// Contact Community Form
	register_sidebar(array(
		'id' => 'contact_community',
		'name' => __( 'Contact Find a group', 'bonestheme' ),
		'description' => __( 'Form from Community Life Page', 'bonestheme' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h4 class="widgettitle">',
		'after_title' => '</h4>'
	));
	
	// Family Night Form
	register_sidebar(array(
		'id' => 'contact_family_night',
		'name' => __( 'Family Night', 'bonestheme' ),
		'description' => __( 'Form from Family Night', 'bonestheme' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h4 class="widgettitle">',
		'after_title' => '</h4>'
	));
	
	// Adoración Form
	register_sidebar(array(
		'id' => 'adoracion',
		'name' => __( 'Formulario Adoracion', 'bonestheme' ),
		'description' => __( 'Formulario Adoracion', 'bonestheme' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h4 class="widgettitle">',
		'after_title' => '</h4>'
	));
	
	// Adoración Form
	register_sidebar(array(
		'id' => 'subscribe-volunteer',
		'name' => __( 'Subscribe volunteer', 'bonestheme' ),
		'description' => __( 'Form volunteer', 'bonestheme' ),
		'before_widget' => '<div class="widget">',
		'after_widget' => '</div>',
		'before_title' => '',
		'after_title' => ''
	));
	
	register_sidebar(array(
		'id' => 'subscribe-voluntarios',
		'name' => __( 'Subscribirte Voluntarios', 'bonestheme' ),
		'description' => __( 'Formulario Subscribirte Voluntarios', 'bonestheme' ),
		'before_widget' => '<div class="widget">',
		'after_widget' => '</div>',
		'before_title' => '',
		'after_title' => ''
	));
	
	// Register Menus 
	function register_my_menus() {
	  register_nav_menus(
		array(
		  'know-menu' => __( 'Know Menu' ),
		  'connect-menu' => __( 'Connect Menu' ),
		  'reach-menu' => __( 'Reach Menu' ),
	  	  'resources-menu' => __( 'Resources Menu' )
		)
	  );
	}
	
} // don't remove this bracket!


/************* COMMENT LAYOUT *********************/

// Comment Layout
function bones_comments( $comment, $args, $depth ) {
   $GLOBALS['comment'] = $comment; ?>
  <div id="comment-<?php comment_ID(); ?>" <?php comment_class('cf'); ?>>
    <article  class="cf">
      <header class="comment-author vcard">
        <?php
        /*
          this is the new responsive optimized comment image. It used the new HTML5 data-attribute to display comment gravatars on larger screens only. What this means is that on larger posts, mobile sites don't have a ton of requests for comment images. This makes load time incredibly fast! If you'd like to change it back, just replace it with the regular wordpress gravatar call:
          echo get_avatar($comment,$size='32',$default='<path_to_url>' );
        */
        ?>
        <?php // custom gravatar call ?>
        <?php
          // create variable
          $bgauthemail = get_comment_author_email();
        ?>
        <img data-gravatar="http://www.gravatar.com/avatar/<?php echo md5( $bgauthemail ); ?>?s=40" class="load-gravatar avatar avatar-48 photo" height="40" width="40" src="<?php echo get_template_directory_uri(); ?>/library/images/nothing.gif" />
        <?php // end custom gravatar call ?>
        <?php printf(__( '<cite class="fn">%1$s</cite> %2$s', 'bonestheme' ), get_comment_author_link(), edit_comment_link(__( '(Edit)', 'bonestheme' ),'  ','') ) ?>
        <time datetime="<?php echo comment_time('Y-m-j'); ?>"><a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ) ?>"><?php comment_time(__( 'F jS, Y', 'bonestheme' )); ?> </a></time>

      </header>
      <?php if ($comment->comment_approved == '0') : ?>
        <div class="alert alert-info">
          <p><?php _e( 'Your comment is awaiting moderation.', 'bonestheme' ) ?></p>
        </div>
      <?php endif; ?>
      <section class="comment_content cf">
        <?php comment_text() ?>
      </section>
      <?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
    </article>
  <?php // </li> is added by WordPress automatically ?>
<?php
} // don't remove this bracket!


/*
This is a modification of a function found in the
bonestheme theme where we can declare some
external fonts. If you're using Google Fonts, you
can replace these fonts, change it in your scss files
and be up and running in seconds.
*/
function bones_fonts() {
  wp_enqueue_style('googleFonts', 'http://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic');
}

add_action('wp_enqueue_scripts', 'bones_fonts');

// Enable support for HTML5 markup.
	add_theme_support( 'html5', array(
		'comment-list',
		'search-form',
		'comment-form'
	) );


/*
* Creating a function to create our CPT
*/

function event_post_type() {

// Set UI labels for Custom Post Type
	$labels = array(
		'name'                => _x( 'Events', 'Post Type General Name', 'bonestheme' ),
		'singular_name'       => _x( 'Event', 'Post Type Singular Name', 'bonestheme' ),
		'menu_name'           => __( 'Events', 'bonestheme' ),
		'parent_item_colon'   => __( 'Parent Event', 'bonestheme' ),
		'all_items'           => __( 'All Events', 'bonestheme' ),
		'view_item'           => __( 'View Event', 'bonestheme' ),
		'add_new_item'        => __( 'Add New Event', 'bonestheme' ),
		'add_new'             => __( 'Add New', 'bonestheme' ),
		'edit_item'           => __( 'Edit Event', 'bonestheme' ),
		'update_item'         => __( 'Update Event', 'bonestheme' ),
		'search_items'        => __( 'Search Event', 'bonestheme' ),
		'not_found'           => __( 'Not Found', 'bonestheme' ),
		'not_found_in_trash'  => __( 'Not found in Trash', 'bonestheme' ),
	);
	
// Set other options for Custom Post Type
	
	$args = array(
		'label'               => __( 'events', 'bonestheme' ),
		'description'         => __( 'Events of Calvary Church', 'bonestheme' ),
		'labels'              => $labels,
		// Features this CPT supports in Post Editor
		'supports'            => array( 'title', 'editor', 'language', 'page-attributes', 'thumbnail' ),
		// You can associate this CPT with a taxonomy or custom taxonomy. 
		'taxonomies'          => array( 'genres' ),
		
		/* A hierarchical CPT is like Pages and can have
		* Parent and child items. A non-hierarchical CPT
		* is like Posts.
		*/	
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
	);
	
	// Registering your Custom Post Type
	register_post_type( 'events', $args );

}


/*
* Creating a function to create our CPT Staff
*/
function staff_post_type() {

// Set UI labels for Custom Post Type
	$labels = array(
		'name'                => _x( 'Staff', 'Post Type General Name', 'bonestheme' ),
		'singular_name'       => _x( 'Staff', 'Post Type Singular Name', 'bonestheme' ),
		'menu_name'           => __( 'Staff', 'bonestheme' ),
		'parent_item_colon'   => __( 'Parent Staff', 'bonestheme' ),
		'all_items'           => __( 'All Staff', 'bonestheme' ),
		'view_item'           => __( 'View Staff', 'bonestheme' ),
		'add_new_item'        => __( 'Add New Staff', 'bonestheme' ),
		'add_new'             => __( 'Add New', 'bonestheme' ),
		'edit_item'           => __( 'Edit Staff', 'bonestheme' ),
		'update_item'         => __( 'Update Staff', 'bonestheme' ),
		'search_items'        => __( 'Search Staff', 'bonestheme' ),
		'not_found'           => __( 'Not Found', 'bonestheme' ),
		'not_found_in_trash'  => __( 'Not found in Trash', 'bonestheme' ),
	);
	
// Set other options for Custom Post Type
	
	$args = array(
		'label'               => __( 'staff', 'bonestheme' ),
		'description'         => __( 'Staff of Calvary Church', 'bonestheme' ),
		'labels'              => $labels,
		// Features this CPT supports in Post Editor
		'supports'            => array( 'title', 'excerpt', 'language', 'thumbnail',  'page-attributes' ),
		// You can associate this CPT with a taxonomy or custom taxonomy. 
		'taxonomies'          => array('category'),
		
		/* A hierarchical CPT is like Pages and can have
		* Parent and child items. A non-hierarchical CPT
		* is like Posts.
		*/	
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
	);
	
	// Registering your Custom Post Type
	register_post_type( 'staff', $args );

}


/*
* Creating a function to create our CPT Staff
*/
function watchseries_post_type() {

// Set UI labels for Custom Post Type
	$labels = array(
		'name'                => _x( 'Series', 'Post Type General Name', 'bonestheme' ),
		'singular_name'       => _x( 'Series', 'Post Type Singular Name', 'bonestheme' ),
		'menu_name'           => __( 'Series', 'bonestheme' ),
		'parent_item_colon'   => __( 'Parent Series', 'bonestheme' ),
		'all_items'           => __( 'All Series', 'bonestheme' ),
		'view_item'           => __( 'View Series', 'bonestheme' ),
		'add_new_item'        => __( 'Add New Series', 'bonestheme' ),
		'add_new'             => __( 'Add New', 'bonestheme' ),
		'edit_item'           => __( 'Edit Series', 'bonestheme' ),
		'update_item'         => __( 'Update Series', 'bonestheme' ),
		'search_items'        => __( 'Search Series', 'bonestheme' ),
		'not_found'           => __( 'Not Found', 'bonestheme' ),
		'not_found_in_trash'  => __( 'Not found in Trash', 'bonestheme' ),
	);
	
// Set other options for Custom Post Type
	
	$args = array(
		'label'               => __( 'series', 'bonestheme' ),
		'description'         => __( 'Series of Calvary Church', 'bonestheme' ),
		'labels'              => $labels,
		// Features this CPT supports in Post Editor
		'supports'            => array( 'title', 'editor', 'excerpt', 'language', 'thumbnail', 'comments', 'revisions' ),
		// You can associate this CPT with a taxonomy or custom taxonomy. 
		'taxonomies'          => array('category'),
		
		/* A hierarchical CPT is like Pages and can have
		* Parent and child items. A non-hierarchical CPT
		* is like Posts.
		*/	
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
	);
	
	// Registering your Custom Post Type
	register_post_type( 'series', $args );

}


// Accesing Languages for CPT
function my_pll_get_post_types($types) {
	return array_merge($types, array(
		'events' => 'events',
		'staff' => 'staff',
		'series' => 'series'
	));
}


/* DON'T DELETE THIS CLOSING TAG */ ?>
