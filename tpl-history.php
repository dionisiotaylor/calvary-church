<?php 
/*
	Template Name: History Calvary Church
*/
?>
<?php get_header(); ?>
	<main role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
		<section class="hero">
			<?php 
				if ( has_post_thumbnail() ) {
					the_post_thumbnail('full');
				} 
			?>
			<div class="hero__wrapper">
				<div class="hero__cont">
					<h2><?php the_field('hero_title'); ?></h2>
					<p><em><?php the_field('hero_description'); ?></em></p>
					<i class="con cross"></i>
				</div>
			</div>
		</section>
		<section class="history">
			<div class="blade red">
				<h2><?php the_field('banner_text'); ?></h2>
			</div>
			<div class="blade featured">
				<img class="full-width-img" src="<?php echo get_template_directory_uri(); ?>/library/images/history-church.jpg" alt="Calvary Church | Connecting People with God" />
				<div class="container">
					<div class="blade__center">
						<h2><?php the_field('date_1_title'); ?></h2>
						<p><?php the_field('date_1_description'); ?></p>
					</div>
				</div>
			</div>
			
			<div class="blade">
				<div class="container">
					<h2><?php the_field('date_2_title'); ?></h2>
					<p><?php the_field('date_2_description'); ?></p>
				</div>
			</div>
			
			<div class="blade">
				<div class="container">
					<h2><?php the_field('date_3_title'); ?></h2>
					<p><?php the_field('date_3_description'); ?></p>
				</div>
			</div>
			
			<div class="blade">
				<div class="container">
					<h2><?php the_field('date_4_title'); ?></h2>
					<p><?php the_field('date_4_description'); ?></p>
				</div>
			</div>
			
			<div class="blade">
				<div class="container">
					<h2><?php the_field('date_5_title'); ?></h2>
					<p><?php the_field('date_5_description'); ?></p>
				</div>
			</div>
			
			<div class="blade">
				<div class="container">
					<h2><?php the_field('date_6_title'); ?></h2>
					<p><?php the_field('date_6_description'); ?></p>
				</div>
			</div>
			
			<div class="blade">
				<div class="container">
					<h2><?php the_field('date_7_title'); ?></h2>
					<p><?php the_field('date_7_description'); ?></p>
				</div>
			</div>
			
			<div class="blade">
				<div class="container">
					<h2><?php the_field('date_8_title'); ?></h2>
					<p><?php the_field('date_8_description'); ?></p>
				</div>
			</div>
			
		</section>
	</main>
<?php get_footer(); ?>
